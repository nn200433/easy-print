# EasyPrint

Lodop + JasperReports 打印模块

## 前言

### Lodop

Lodop是什么？

有人说她是Web打印控件，因为她能打印、在浏览器中以插件的形式出现，用简单一行语句就把整个网页打印出来；
有人说她是打印编程接口，因为她介于浏览器和打印设备之间，是个通道和桥梁，几乎能想到的打印控制事项都能做到；
有人说她是JavaScript的扩展，因为她所有功能就那么几个语句，和JS语法一样，一看就明白个究竟；
有人说她是报表打印工具，因为那个add_print_table语句把报表统计的那点事弄了个明明白白；
有人说她是条码打印工具，因为用了她再也不用后台生成条码图片了，前端一行指令就动态输出清晰准确的条码，一维二维都行；
有人说她是图表打印工具，因为用她能输出几乎能想象的任何图表，虽然没那么豪华，但什么饼图、折线图、柱图甚至复合图等等都不在话下；
有人说她是个小玩意，因为她体积太小了，才2M多，她所包含的其中任何一个对照工具都是她的好几倍（例如条码打印控件、图表控件等）；
有人说她是套打教案，因为以Lodop+JS实现套打这种模式，在网上已被吵吵为教科书般的解决方案；
有人说她是Web打印控件的“终结者”，因为接触“她”后再不想别的“她”；
有人说她就是一个Web编程小工具，因为有了她，在BS下的打印终于像cs下那种随意而高效了；
但我们说，她是全国1000多家软件公司的智慧结晶，诞生10年了，几乎每个功能细节都蕴藏着无数开发者的期待和汗水；
她就是Lodop(读音“劳道谱”)，没有别的名称，她是web开发的必选伴侣；
现在，她又添了个小兄弟，名叫C-Lodop（可编程的云打印），未来将由他开创......

相关链接：

* 官网：[跳转地址](http://www.lodop.net/index.html)

### JasperReports

JasperReports是一个基于Java的开源报表工具，它可以在Java环境下像其它IDE报表工具一样来制作报表。JasperReports 支持PDF、HTML、XLS、CSV和XML文件输出格式。JasperReports是当前Java开发者最常用的报表工具。

### LINUX系统下PDF字体异常

将 `doc/fonts` 下的字体上传至 `/usr/share/fonts` 下

### 相关链接

* [开发手册](./doc/JasperReport)
* [报表jar包](https://community.jaspersoft.com/project/jasperreports-library)
* [报表设计器](https://community.jaspersoft.com/project/jaspersoft-studio)

## 项目结构

```
EasyPrint
├─ main
│  ├─ java
│  │  └─ cn
│  │   └─ nn200433
│  │    └─ print 
│  │     ├─ config
│  │     │  ├─ PrintProperties.java                                 application.yml
│  │     │  └─ PrintStarterAutoConfig.java                          初始化工具类，注入配置
│  │     ├─ constants
│  │     │  └─ PrintConstant.java                                   常量类
│  │     ├─ entity
│  │     │  ├─ HtmlRenderData.java                                  模板填充数据对象
│  │     │  ├─ HtmlRenderDataSource.java                            模板数据来源 枚举类
│  │     │  ├─ PaperStyle.java                                      纸张样式 枚举类
│  │     │  └─ PrintConfig.java                                     打印风格对象（如纸张样式，内容边距......）
│  │     ├─ Printer.java                                            lodop打印调用类（组装打印代码供前端使用）
│  │     ├─ service
│  │     │  ├─ B64ImgReplacedElementFactory.java
│  │     │  ├─ BaseRenderFactoryHandler.java
│  │     │  └─ customer                                             自定义化接口
│  │     │   ├─ impl
│  │     │   │  ├─ DefaultJasperReportDataListCoverImpl.java        dataList 的默认 JasperReportParametersCover实现
│  │     │   │  └─ DefaultJasperReportParametersMapCoverImpl.java   parametersMap 的默认 JasperReportParametersCover实现
│  │     │   └─ JasperReportParametersCover.java                    jasperReport报表参数覆盖
│  │     └─ util
│  │      ├─ HtmlRenderFactoryUtil.java                             html渲染工具类
│  │      ├─ PdfRenderFactoryUtil.java                              pdf渲染工具类
│  │      ├─ OfdRenderFactoryUtil.java                              ofd渲染工具类（《GB/T 33190-2016电子文件存储与交换格式-版式文档》）
│  │      └─ SpringContextHolder.java
│  └─ resources
│   ├─ application.yml                                              配置文件
│   ├─ print-template                                               模板文件目录
│   │  ├─ demo4.html                                                - 测试样例（beetl）
│   │  └─ Test_A4.jrxml                                             - 测试样例（JasperReport）
│   └─ static
│    ├─ lodop_install                                               打印控件下载路径
│    │  ├─ CLodop_Setup_for_Win32NT.exe
│    │  ├─ install_lodop32.exe
│    │  └─ install_lodop64.exe
│    └─ vendors
│     ├─ html2canvas                                                html转图片插件
│     │  ├─ html2canvas.js
│     │  └─ html2canvas.min.js
│     └─ lodop                                                      lodop打印插件
│      ├─ LodopFuncs.js                                             - lodop打印核心代码
│      └─ Lodop_print.js                                            - 自定义调用方法
└─ test                                                             DEMO运行示例
```

## 开发教程

### 引入jar包

```xml
<!-- 自行编译 -->
<dependency>
    <groupId>cn.nn200433</groupId>
    <artifactId>EasyPrint</artifactId>
    <version>${latestVersion}</version>
</dependency>

<!-- hutool工具包 -->
<dependency>
    <groupId>cn.hutool</groupId>
    <artifactId>hutool-all</artifactId>
    <version>${hutool.version}</version>
</dependency>

<!-- JSON工具类 -->
<dependency>
    <groupId>com.alibaba</groupId>
    <artifactId>fastjson</artifactId>
    <version>${fastjson.version}</version>
</dependency>
```

### 配置

```yaml
print:
    # 传入打印组件的数据进行反转义
  dataUnescape: true
  # 模板路径，默认
  templatePath:
```

### 工具类

```java
// ========================================= HtmlRenderFactoryUtil =========================================

/**
 * 套打模式渲染
 * 自定义数据并渲染为模板
 *
 * @param templateName    模板名称
 * @param htmlRenderData  填充数据（若dataList为空，则采用druid数据源）
 * @param subTemplateName 子模板名称（在jasperReport中，添加相同个数名称为“SUB_REPORT_[0，1，2......]”；Class属性为“net.sf.jasperreports.engine.JasperReport”的Parameters参数）
 * @return {@link String}
 */
public static List<String> renderList(String templateName, 
                                      HtmlRenderData htmlRenderData, 
                                      int pageDataSize,
									  String... subTemplateName);

/**
 * 套打模式渲染
 * 自定义数据并渲染为模板
 *
 * @param templateName        模板名称
 * @param htmlRenderData      填充数据（若dataList为空，则采用druid数据源）
 * @param pageDataSize        每页打印数据量
 * @param customerParamMapFun 自定义每页的参数映射处理
 * @param customerDataListFun 自定义每页的数据列表处理
 * @param subTemplateName     子模板名称（在jasperReport中，添加相同个数名称为“SUB_REPORT_[0，1，2......]”；Class属性为“net.sf.jasperreports.engine.JasperReport”的Parameters参数）
 * @return {@link String}
 */
public static List<String> renderList(String templateName, 
                                      HtmlRenderData htmlRenderData, 
                                      int pageDataSize,
									  JasperReportParametersCover<Map<String, Object>> customerParamMapFun,
									  JasperReportParametersCover<List<Map<String, ?>>> customerDataListFun,
									  String... subTemplateName);

/**
 * 渲染（使用默认html处理程序）
 * 自定义数据并渲染为模板
 *
 * @param templateName    模板名称
 * @param htmlRenderData  填充数据（若dataList为空，则采用druid数据源）
 * @param subTemplateName 子模板名称（在jasperReport中，添加相同个数名称为“SUB_REPORT_[0，1，2......]”；Class属性为“net.sf.jasperreports.engine.JasperReport”的Parameters参数）
 * @return {@link String}
 */
public static String render(String templateName, HtmlRenderData htmlRenderData, String... subTemplateName);

/**
 * 渲染
 * 自定义数据并渲染为模板
 *
 * @param templateName        模板名称
 * @param htmlRenderData      填充数据（若dataList为空，则采用druid数据源）
 * @param customerParamMapFun 自定义参数映射处理。不传且 print.data.unescape 打印数据反转义启用时，使用默认实现 {@link cn.hf.ext.print.service.customer.impl.DefaultJasperReportParametersMapCoverImpl}
 * @param customerDataListFun 自定义数据列表处理，不传且 print.data.unescape 打印数据反转义启用时，使用默认实现 {@link cn.hf.ext.print.service.customer.impl.DefaultJasperReportDataListCoverImpl}
 * @param subTemplateName     子模板名称（在jasperReport中，添加相同个数名称为“SUB_REPORT_[0，1，2......]”；Class属性为“net.sf.jasperreports.engine.JasperReport”的Parameters参数）
 * @return {@link String}
 */
public static String render(String templateName,
							HtmlRenderData htmlRenderData,
							JasperReportParametersCover<Map<String, Object>> customerParamMapFun,
							JasperReportParametersCover<List<Map<String, ?>>> customerDataListFun,
							String... subTemplateName);

/**
 * 使用beetl渲染
 *
 * @param templateName 模板名称
 * @param dataMap      数据
 * @return {@link String }
 * @author sjx
 * @date 2020年09月30日 16:24:01
 */
public static String render(String templateName, Map<String, Object> dataMap);
/**
 * 使用beetl渲染
 *
 * @param templateName       模板名称
 * @param dataMap            数据
 * @param customerDataMapFun 自定义数据对象处理
 * @return {@link String }
 * @author sjx
 * @date 2020年09月30日 16:24:01
 */
public static String render(String templateName, Map<String, Object> dataMap, JasperReportParametersCover<Map<String, Object>> customerDataMapFun);
							
// ========================================= PdfRenderFactoryUtil =========================================

/**
 * 渲染
 *
 * @param templateName    模板名称
 * @param htmlRenderData  填充数据（若dataList为空，则采用druid数据源）
 * @param subTemplateName 子模板名称（在jasperReport中，添加相同个数名称为“SUB_REPORT_[0，1，2......]”；Class属性为“net.sf.jasperreports.engine.JasperReport”的Parameters参数）
 * @return @return {@link File }
 */
public static File render(String templateName, HtmlRenderData htmlRenderData, String... subTemplateName);

/**
 * 渲染
 * 自定义数据并渲染为模板
 *
 * @param templateName           模板名称
 * @param htmlRenderData         填充数据（若dataList为空，则采用druid数据源）
 * @param customerParamMapStrFun 自定义数据对象转为JSON字符串后的处理
 * @param customerDataListStrFun 自定义数据对象转为JSON字符串后的处理
 * @param subTemplateName        子模板名称（在jasperReport中，添加相同个数名称为“SUB_REPORT_[0，1，2......]”；Class属性为“net.sf.jasperreports.engine.JasperReport”的Parameters参数）
 * @return @return {@link File }
 */
public static File render(String templateName,
						  HtmlRenderData htmlRenderData,
						  JasperReportParametersCover<Map<String, Object>> customerParamMapStrFun,
						  JasperReportParametersCover<List<Map<String, ?>>> customerDataListStrFun,
						  String... subTemplateName);

/**
 * 渲染
 *
 * @param templateName    模板名称
 * @param htmlRenderData  填充数据（若dataList为空，则采用druid数据源）
 * @param subTemplateName 子模板名称（在jasperReport中，添加相同个数名称为“SUB_REPORT_[0，1，2......]”；Class属性为“net.sf.jasperreports.engine.JasperReport”的Parameters参数）
 * @return @return {@link byte[] }
 */
public static byte[] render2Bytes(String templateName, HtmlRenderData htmlRenderData, String... subTemplateName);

/**
 * 渲染
 * 自定义数据并渲染为模板
 *
 * @param templateName           模板名称
 * @param htmlRenderData         填充数据（若dataList为空，则采用druid数据源）
 * @param customerParamMapStrFun 自定义数据对象转为JSON字符串后的处理
 * @param customerDataListStrFun 自定义数据对象转为JSON字符串后的处理
 * @param subTemplateName        子模板名称（在jasperReport中，添加相同个数名称为“SUB_REPORT_[0，1，2......]”；Class属性为“net.sf.jasperreports.engine.JasperReport”的Parameters参数）
 * @return @return {@link byte[] }
 */
public static byte[] render2Bytes(String templateName,
								  HtmlRenderData htmlRenderData,
								  JasperReportParametersCover<Map<String, Object>> customerParamMapStrFun,
								  JasperReportParametersCover<List<Map<String, ?>>> customerDataListStrFun,
								  String... subTemplateName);

/**
 * html转pdf
 *
 * <pre>
 * 注：默认只处理宋体
 * 页面中字体不能使用中文，需要使用英文名称，而且是大小写敏感的！例如宋体的英文名称是 SimSun(注意不是simsun！，首字母都是大写的)
 * 错误写法：font-family:宋体 或者  font-family:simsun
 * 正确写法：font-family:SimSun 或者 font-family:SimHei
 * </pre>
 *
 * @param htmlString html字符串
 * @return @return {@link File }
 */
public static File htmlToPdf(String htmlString);

/**
 * html转pdf
 *
 * <pre>
 * 注：默认只处理宋体
 * 页面中字体不能使用中文，需要使用英文名称，而且是大小写敏感的！例如宋体的英文名称是 SimSun(注意不是simsun！，首字母都是大写的)
 * 错误写法：font-family:宋体 或者  font-family:simsun
 * 正确写法：font-family:SimSun 或者 font-family:SimHei
 * </pre>
 *
 * @param htmlString           html字符串
 * @param isInsideGenerateHtml 是否内部生成的Html，HtmlRenderFactoryUtil产生的html为内部生成（做过特殊处理）
 * @return @return {@link File }
 */
public static File htmlToPdf(String htmlString, Boolean isInsideGenerateHtml);

/**
 * html转pdf
 *
 * <pre>
 * 注：默认只处理宋体
 * 页面中字体不能使用中文，需要使用英文名称，而且是大小写敏感的！例如宋体的英文名称是 SimSun(注意不是simsun！，首字母都是大写的)
 * 错误写法：font-family:宋体 或者  font-family:simsun
 * 正确写法：font-family:SimSun 或者 font-family:SimHei
 * </pre>
 *
 * @param htmlString html字符串
 * @return @return {@link File }
 */
public static byte[] htmlToPdfBytes(String htmlString);

/**
 * html转pdf
 *
 * <pre>
 * 注：默认只处理宋体
 * 页面中字体不能使用中文，需要使用英文名称，而且是大小写敏感的！例如宋体的英文名称是 SimSun(注意不是simsun！，首字母都是大写的)
 * 错误写法：font-family:宋体 或者  font-family:simsun
 * 正确写法：font-family:SimSun 或者 font-family:SimHei
 * </pre>
 *
 * @param htmlString           html字符串
 * @param isInsideGenerateHtml 是否内部生成的Html，HtmlRenderFactoryUtil产生的html为内部生成（做过特殊处理）
 * @return @return {@link byte[] }
 */
public static byte[] htmlToPdfBytes(String htmlString, Boolean isInsideGenerateHtml);

// ========================================= OfdRenderFactoryUtil =========================================

/**
 * 渲染（内部发pdf转ofd）
 *
 * @param templateName    模板名称
 * @param htmlRenderData  填充数据（若dataList为空，则采用druid数据源）
 * @param subTemplateName 子模板名称（在jasperReport中，添加相同个数名称为“SUB_REPORT_[0，1，2......]”；Class属性为“net.sf.jasperreports.engine.JasperReport”的Parameters参数）
 * @return @return {@link File }
 */
public static File render(String templateName, HtmlRenderData htmlRenderData, String... subTemplateName);

/**
 * 渲染（内部发pdf转ofd）
 * 自定义数据并渲染为模板
 *
 * @param templateName           模板名称
 * @param htmlRenderData         填充数据（若dataList为空，则采用druid数据源）
 * @param customerParamMapStrFun 自定义数据对象转为JSON字符串后的处理
 * @param customerDataListStrFun 自定义数据对象转为JSON字符串后的处理
 * @param subTemplateName        子模板名称（在jasperReport中，添加相同个数名称为“SUB_REPORT_[0，1，2......]”；Class属性为“net.sf.jasperreports.engine.JasperReport”的Parameters参数）
 * @return @return {@link File }
 */
public static File render(String templateName,
						  HtmlRenderData htmlRenderData,
						  JasperReportParametersCover<Map<String, Object>> customerParamMapStrFun,
						  JasperReportParametersCover<List<Map<String, ?>>> customerDataListStrFun,
						  String... subTemplateName);

/**
 * 渲染（内部发pdf转ofd）
 *
 * @param templateName    模板名称
 * @param htmlRenderData  填充数据（若dataList为空，则采用druid数据源）
 * @param subTemplateName 子模板名称（在jasperReport中，添加相同个数名称为“SUB_REPORT_[0，1，2......]”；Class属性为“net.sf.jasperreports.engine.JasperReport”的Parameters参数）
 * @return @return {@link byte[] }
 */
public static byte[] render2Bytes(String templateName, HtmlRenderData htmlRenderData, String... subTemplateName);

/**
 * 渲染（内部发pdf转ofd）
 * 自定义数据并渲染为模板
 *
 * @param templateName           模板名称
 * @param htmlRenderData         填充数据（若dataList为空，则采用druid数据源）
 * @param customerParamMapStrFun 自定义数据对象转为JSON字符串后的处理
 * @param customerDataListStrFun 自定义数据对象转为JSON字符串后的处理
 * @param subTemplateName        子模板名称（在jasperReport中，添加相同个数名称为“SUB_REPORT_[0，1，2......]”；Class属性为“net.sf.jasperreports.engine.JasperReport”的Parameters参数）
 * @return @return {@link byte[] }
 */
public static byte[] render2Bytes(String templateName,
								  HtmlRenderData htmlRenderData,
								  JasperReportParametersCover<Map<String, Object>> customerParamMapStrFun,
								  JasperReportParametersCover<List<Map<String, ?>>> customerDataListStrFun,
								  String... subTemplateName);

/**
 * html转ofd（内部生成html转换为pdf文件，最后再转为ofd文件）
 *
 * <pre>
 * 注：默认只处理宋体
 * 页面中字体不能使用中文，需要使用英文名称，而且是大小写敏感的！例如宋体的英文名称是 SimSun(注意不是simsun！，首字母都是大写的)
 * 错误写法：font-family:宋体 或者  font-family:simsun
 * 正确写法：font-family:SimSun 或者 font-family:SimHei
 * </pre>
 *
 * @param htmlString html字符串
 * @return @return {@link File }
 */
public static File htmlToOfd(String htmlString);

/**
 * html转ofd（内部生成html转换为pdf文件，最后再转为ofd文件）
 *
 * <pre>
 * 注：默认只处理宋体
 * 页面中字体不能使用中文，需要使用英文名称，而且是大小写敏感的！例如宋体的英文名称是 SimSun(注意不是simsun！，首字母都是大写的)
 * 错误写法：font-family:宋体 或者  font-family:simsun
 * 正确写法：font-family:SimSun 或者 font-family:SimHei
 * </pre>
 *
 * @param htmlString           html字符串
 * @param isInsideGenerateHtml 是否内部生成的Html，HtmlRenderFactoryUtil产生的html为内部生成（做过特殊处理）
 * @return @return {@link File }
 */
public static File htmlToOfd(String htmlString, Boolean isInsideGenerateHtml);

/**
 * html转ofd（内部生成html转换为pdf文件，最后再转为ofd文件）
 *
 * <pre>
 * 注：默认只处理宋体
 * 页面中字体不能使用中文，需要使用英文名称，而且是大小写敏感的！例如宋体的英文名称是 SimSun(注意不是simsun！，首字母都是大写的)
 * 错误写法：font-family:宋体 或者  font-family:simsun
 * 正确写法：font-family:SimSun 或者 font-family:SimHei
 * </pre>
 *
 * @param htmlString           html字符串
 * @param isInsideGenerateHtml 是否内部生成的Html，HtmlRenderFactoryUtil产生的html为内部生成（做过特殊处理）
 * @return @return {@link byte[] }
 */
public static byte[] htmlToOfdBytes(String htmlString, Boolean isInsideGenerateHtml);

/**
 * html转ofd（内部生成html转换为pdf文件，最后再转为ofd文件）
 *
 * <pre>
 * 注：默认只处理宋体
 * 页面中字体不能使用中文，需要使用英文名称，而且是大小写敏感的！例如宋体的英文名称是 SimSun(注意不是simsun！，首字母都是大写的)
 * 错误写法：font-family:宋体 或者  font-family:simsun
 * 正确写法：font-family:SimSun 或者 font-family:SimHei
 * </pre>
 *
 * @param htmlString html字符串
 * @return @return {@link File }
 */
public static byte[] htmlToOfdBytes(String htmlString);
```

### 安装软件

* [JaspersoftStudio-6.12.2](https://nchc.dl.sourceforge.net/project/jasperstudio/JaspersoftStudio-6.12.2/TIB_js-studiocomm_6.12.2_windows_x86_64.zip)
* [Lodop6.226](http://www.lodop.net/download/Lodop6.226_Clodop4.088.zip)

*注：JaspersoftStudio使用教程请参考根目录doc文件夹下的pdf文档。iReport是JaspersoftStudio的前身，用法几乎一致。*

### JasperReport模板

在`resource`下新建`print-template`打印模板存放目录。

模板文件如下：

Test_A4.jrxml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!-- Created with Jaspersoft Studio version 6.12.2.final using JasperReports Library version 6.12.2-75c5e90a222ab406e416cbf590a5397028a52de3  -->
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="Test_A4" pageWidth="595" pageHeight="842" columnWidth="555" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20" uuid="262435ae-66a3-4509-ac68-e163b333f6d3">
	<property name="com.jaspersoft.studio.data.defaultdataadapter" value="One Empty Record"/>
	<parameter name="TABLE_TITLE" class="java.lang.String">
		<parameterDescription><![CDATA[标题]]></parameterDescription>
	</parameter>
	<queryString>
		<![CDATA[]]>
	</queryString>
	<field name="FIELD_ACCOUNT" class="java.lang.String">
		<fieldDescription><![CDATA[帐号]]></fieldDescription>
	</field>
	<field name="FIELD_REAL_NAME" class="java.lang.String">
		<fieldDescription><![CDATA[真实姓名]]></fieldDescription>
	</field>
	<background>
		<band splitType="Prevent"/>
	</background>
	<title>
		<band height="60" splitType="Stretch">
			<textField>
				<reportElement x="180" y="0" width="200" height="40" uuid="d07e7265-85f8-4e43-8050-5573734fd1e9"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="宋体" size="21"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{TABLE_TITLE}]]></textFieldExpression>
			</textField>
		</band>
	</title>
	<columnHeader>
		<band height="30" splitType="Stretch">
			<staticText>
				<reportElement x="0" y="0" width="280" height="30" uuid="67e869d1-74ec-4668-8e98-ab9987a37dae"/>
				<box>
					<topPen lineWidth="1.0"/>
					<leftPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
					<rightPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="宋体" size="18" isBold="true"/>
				</textElement>
				<text><![CDATA[帐号]]></text>
			</staticText>
			<staticText>
				<reportElement x="280" y="0" width="275" height="30" uuid="431d78fe-1515-483a-8bd3-6ad5fd8ae4c5"/>
				<box>
					<topPen lineWidth="1.0"/>
					<leftPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
					<rightPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="宋体" size="18" isBold="true"/>
				</textElement>
				<text><![CDATA[真实姓名]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="30" splitType="Stretch">
			<textField>
				<reportElement x="0" y="0" width="280" height="30" uuid="63eb86bb-3eb1-43d4-93df-4f70f23f4446"/>
				<box>
					<topPen lineWidth="1.0"/>
					<leftPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
					<rightPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="宋体" size="18"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{FIELD_ACCOUNT}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="280" y="0" width="275" height="30" uuid="4dae300a-668c-4f21-91a9-bc8c0d692477"/>
				<box>
					<topPen lineWidth="1.0"/>
					<leftPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
					<rightPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="宋体" size="18"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{FIELD_REAL_NAME}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
</jasperReport>
```

*注：*

*1. 主表中每增加一个子表需要在`Parameters`下新增一个`SUB_REPORT_[0，1，2.....]`的`net.sf.jasperreports.engine.JasperReport`对象参数。*

*2. 子表的数据可以定义在`Parameters`下，参数类型为`Map`或`List`*

### 后端代码

关键代码：

```java
// 模板渲染
String html = HtmlRenderFactoryUtil.render("Test_A4",
        new HtmlRenderData(paramMaps, detailList));

System.out.println("==================== JasperReport模板渲染后的Html代码 ====================");
System.out.println(html);

// 打印参数配置
PrintConfig printConfig = new PrintConfig(html);

// 生成打印控件需要的代码
String str = new Printer(printConfig).print();

System.out.println("==================== Lodop打印javascript脚本语句 ====================");
System.out.println(str);
```

举个web调用例子：

```java
/**
 * 打印
 *
 * @param id id 记录ID
 * @return {@link String}
 */
@PostMapping("{id}/print")
public Response print(@PathVariable("id") String id) {
    Response response = null;
    try {
        // 获取待渲染的数据
        Map<String,Object> dataMap = printData(id);
        // 生成html表格
        String html = HtmlRenderFactoryUtil.render("hospitality",
                new HtmlRenderData(
                    MapUtil.get(dataMap, "entertainmentExpensesMap", new TypeReference<Map<String, Object>>() {}),
                    HtmlRenderDataSource.DATA_FROM_COLLECTION_MAP
                )
        );
        
        // 打印配置，如横向/纵向
        // 不设置纸张样式时，默认是 A4 但 长宽全是 0
        PrintConfig printConfig = new PrintConfig(html);

        response = Response.ok();        
        // new Printer(printConfig).print() 该语句将生成调用Lodop控件的脚本
        response.put("expression", new Printer(printConfig).print());
    } catch (Exception e) {
        logger.error("======> 打印出错", e);
        response = Response.error(e.getMessage());
    }

    return response;
}
```

详细方法用例：

```java
package cn.nn200433.print;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.date.TimeInterval;
import cn.hutool.core.lang.Console;
import Printer;
import HtmlRenderData;
import PrintConfig;
import JasperReportParametersCover;
import HtmlRenderFactoryUtil;
import PdfRenderFactoryUtil;
import com.forte.util.utils.MockUtil;
import org.junit.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 打印测试
 *
 * @author sjx
 * @date 2020年05月18日 0018 15:05:05
 */
public class PrintTest {

    /**
     * 示例1 - jasperReport导出html使用示例
     *
     * @throws Exception 异常
     */
    @Test
    public void demo1() throws Exception {
        TimeInterval timer = DateUtil.timer();

        Map<String, Object> paramMaps = new HashMap<String, Object>(1);
        paramMaps.put("TABLE_TITLE", "测试标题");

        List<Map<String, ?>> detailList = new ArrayList<Map<String, ?>>(20);
        for (int i = 0; i < 20; i++) {
            Map<String, Object> tempMap = new HashMap<String, Object>();
            tempMap.put("FIELD_ACCOUNT", MockUtil.title(5));
            tempMap.put("FIELD_REAL_NAME", MockUtil.cname());
            detailList.add(tempMap);
        }

        // 模板渲染
        String html = HtmlRenderFactoryUtil.render("Test_A4",
                new HtmlRenderData(paramMaps, detailList), new JasperReportParametersCover<Map<String, Object>>() {
                    @Override
                    public Map<String, Object> cover(Map<String, Object> parameterMap) throws Exception {
                        return parameterMap;
                    }
                }, new JasperReportParametersCover<List<Map<String, ?>>>() {
                    @Override
                    public List<Map<String, ?>> cover(List<Map<String, ?>> dataList) throws Exception {
                        return dataList;
                    }
                });

        System.out.println("==================== JasperReport模板渲染后的Html代码 ====================");
        System.out.println(html);

        // 打印参数配置
        PrintConfig printConfig = new PrintConfig(html);

        // 生成打印控件需要的代码
        String str = new Printer(printConfig).print();

        System.out.println("==================== Lodop打印javascript脚本语句 ====================");
        System.out.println(str);

        System.out.println("打印运行耗时 " + timer.intervalSecond() + " 秒");
    }

    /**
     * 示例2 - jasperReport导出pdf示例
     *
     * @return
     */
    @Test
    public void demo2() {
        TimeInterval timer = DateUtil.timer();

        Map<String, Object> paramMaps = new HashMap<String, Object>(1);
        paramMaps.put("TABLE_TITLE", "测试&bull;标题");

        List<Map<String, ?>> detailList = new ArrayList<Map<String, ?>>(20);
        for (int i = 0; i < 20; i++) {
            Map<String, Object> tempMap = new HashMap<String, Object>();
            tempMap.put("FIELD_ACCOUNT", MockUtil.title(5) + "&gt;");
            tempMap.put("FIELD_REAL_NAME", MockUtil.cname());
            detailList.add(tempMap);
        }

        // 模板渲染
        File pdfFile = PdfRenderFactoryUtil.render("Test_A4",
                new HtmlRenderData(paramMaps, detailList));

        Console.log("======> 文件路径 = {}", pdfFile.getAbsolutePath());

        System.out.println("打印运行耗时 " + timer.intervalSecond() + " 秒");
    }

    /**
     * 示例3 - html转pdf
     *
     * @return
     */
    @Test
    public void demo3() {
        // 字体只处理宋体！！！！

        String s = "<!DOCTYPE html PUBLIC\"-//W3C//DTD XHTML 1.0 Transitional//EN\"\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\"><html><head><title></title><meta http-equiv=\"Content-Type\"content=\"text/html; charset=UTF-8\"/><style type=\"text/css\">a{text-decoration:none}</style></head><body text=\"#000000\"link=\"#000000\"alink=\"#000000\"vlink=\"#000000\"><table width=\"100%\"cellpadding=\"0\"cellspacing=\"0\"border=\"0\"><tr><td width=\"50%\">&nbsp;</td><td align=\"center\"><a name=\"JR_PAGE_ANCHOR_0_1\"></a><table class=\"jrPage\"cellpadding=\"0\"cellspacing=\"0\"border=\"0\"style=\"empty-cells: show; width: 595px; border-collapse: collapse; background-color: white;\"><tr valign=\"top\"style=\"height:0\"><td style=\"width:20px\"></td><td style=\"width:180px\"></td><td style=\"width:100px\"></td><td style=\"width:100px\"></td><td style=\"width:175px\"></td><td style=\"width:20px\"></td></tr><tr valign=\"top\"style=\"height:20px\"><td colspan=\"6\"></td></tr><tr valign=\"top\"style=\"height:40px\"><td colspan=\"2\"></td><td colspan=\"2\"style=\"text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 21px; line-height: 1.140625;\">测试标题</span></td><td colspan=\"2\"></td></tr><tr valign=\"top\"style=\"height:20px\"><td colspan=\"6\"></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625; font-weight: bold;\">帐号</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625; font-weight: bold;\">真实姓名</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">Vtvjo</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">席皂斋</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">Ltoix</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">连雪空</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">Jazsj</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">屈速</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">Tzans</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">谷梁成</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">Eqakb</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">寿呈</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">Izgmw</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">萧菜铣</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">Vizmk</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">隆噶</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">Nhbkv</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">宦譬</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">Hhpzy</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">郇欠放</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">Xyupx</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">蔺换稍</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">Usczd</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">祖玩该</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">Wkfkc</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">韶芥写</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">Qgrvq</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">綦毋恋</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">Dmkmb</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">鲁驯</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">Yzlsc</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">籍仿</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">Nfwkd</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">都筒</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">Nsegj</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">莘喇距</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">Mvnln</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">覃知</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">Tkbkb</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">谈酸</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">Zynak</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">边捷</span></td><td></td></tr><tr valign=\"top\"style=\"height:132px\"><td colspan=\"6\"></td></tr></table></td><td width=\"50%\">&nbsp;</td></tr></table></body></html>";
        File file = PdfRenderFactoryUtil.htmlToPdf(s, Boolean.FALSE);
        Console.log("======> 文件路径1 = {}", file.getAbsolutePath());
        // 该 html 的 style中的 font-family: \'宋体\' 经过特殊处理，需注意
        String s1 = "<!DOCTYPE html PUBLIC\"-//W3C//DTD XHTML 1.0 Transitional//EN\"\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\"><html><head><title></title><meta http-equiv=\"Content-Type\"content=\"text/html; charset=UTF-8\"/><style type=\"text/css\">a{text-decoration:none}</style></head><body text=\"#000000\"link=\"#000000\"alink=\"#000000\"vlink=\"#000000\"><table width=\"100%\"cellpadding=\"0\"cellspacing=\"0\"border=\"0\"><tr><td width=\"50%\">&nbsp;</td><td align=\"center\"><a name=\"JR_PAGE_ANCHOR_0_1\"></a><table class=\"jrPage\"cellpadding=\"0\"cellspacing=\"0\"border=\"0\"style=\"empty-cells: show; width: 595px; border-collapse: collapse; background-color: white;\"><tr valign=\"top\"style=\"height:0\"><td style=\"width:20px\"></td><td style=\"width:180px\"></td><td style=\"width:100px\"></td><td style=\"width:100px\"></td><td style=\"width:175px\"></td><td style=\"width:20px\"></td></tr><tr valign=\"top\"style=\"height:20px\"><td colspan=\"6\"></td></tr><tr valign=\"top\"style=\"height:40px\"><td colspan=\"2\"></td><td colspan=\"2\"style=\"text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 21px; line-height: 1.140625;\">测试标题</span></td><td colspan=\"2\"></td></tr><tr valign=\"top\"style=\"height:20px\"><td colspan=\"6\"></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625; font-weight: bold;\">帐号</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625; font-weight: bold;\">真实姓名</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">Vtvjo</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">席皂斋</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">Ltoix</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">连雪空</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">Jazsj</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">屈速</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">Tzans</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">谷梁成</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">Eqakb</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">寿呈</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">Izgmw</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">萧菜铣</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">Vizmk</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">隆噶</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">Nhbkv</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">宦譬</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">Hhpzy</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">郇欠放</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">Xyupx</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">蔺换稍</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">Usczd</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">祖玩该</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">Wkfkc</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">韶芥写</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">Qgrvq</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">綦毋恋</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">Dmkmb</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">鲁驯</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">Yzlsc</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">籍仿</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">Nfwkd</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">都筒</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">Nsegj</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">莘喇距</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">Mvnln</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: '\\'宋体\\''; color: #000000; font-size: 18px; line-height: 1.140625;\">覃知</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">Tkbkb</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">谈酸</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">Zynak</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">边捷</span></td><td></td></tr><tr valign=\"top\"style=\"height:132px\"><td colspan=\"6\"></td></tr></table></td><td width=\"50%\">&nbsp;</td></tr></table></body></html>";
        File file1 = PdfRenderFactoryUtil.htmlToPdf(s1);
        Console.log("======> 文件路径2 = {}", file1.getAbsolutePath());
    }

    /**
     * 示例4 - 使用beetl模板引擎
     *
     * @throws Exception 异常
     */
    @Test
    public void demo4() throws Exception {
        TimeInterval timer = DateUtil.timer();

        Map<String, Object> paramMaps = new HashMap<String, Object>(1);
        paramMaps.put("title", "测试标题");
        paramMaps.put("date", "2020-01-01 至 2020-02-28");

        List<Map<String, Object>> dataList = new ArrayList<Map<String, Object>>(3);
        for (int i = 0; i < 5; i++) {
            HashMap<String, Object> stringObjectHashMap = new HashMap<>();
            stringObjectHashMap.put("orgPosition", "测试" + i);
            stringObjectHashMap.put("opinion", "测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试");
            stringObjectHashMap.put("createByName", "姓名_" + i);
            stringObjectHashMap.put("createDateStr", "2020-09-30");
            dataList.add(stringObjectHashMap);
        }
        paramMaps.put("dataList", dataList);
        // 模板渲染
        String html = HtmlRenderFactoryUtil.render("demo4.html", paramMaps);

        System.out.println("==================== beetl模板渲染后的Html代码 ====================");
        System.out.println(html);

        // 打印参数配置
        PrintConfig printConfig = new PrintConfig(html);

        // 生成打印控件需要的代码
        String str = new Printer(printConfig).print();

        System.out.println("==================== Lodop打印javascript脚本语句 ====================");
        System.out.println(str);

        System.out.println("打印运行耗时 " + timer.intervalSecond() + " 秒");
    }

    /**
     * 示例5 - 套打
     *
     * @throws Exception 异常
     */
    @Test
    public void demo5() throws Exception {
        TimeInterval timer = DateUtil.timer();

        Map<String, Object> paramMaps = new HashMap<String, Object>(1);
        paramMaps.put("TABLE_TITLE", "测试标题");

        List<Map<String, ?>> detailList = new ArrayList<Map<String, ?>>(20);
        for (int i = 0; i < 5; i++) {
            Map<String, Object> tempMap = new HashMap<String, Object>();
            tempMap.put("FIELD_ACCOUNT", MockUtil.title(5));
            tempMap.put("FIELD_REAL_NAME", MockUtil.cname());
            detailList.add(tempMap);
        }

        // 模板渲染
        List<String> htmlList = HtmlRenderFactoryUtil.renderList("Test_A4",
                new HtmlRenderData(paramMaps, detailList), 4,
                new JasperReportParametersCover<Map<String, Object>>() {
                    @Override
                    public Map<String, Object> cover(Map<String, Object> paramMap) throws Exception {
                        paramMap.put("TABLE_TITLE", "测试标题2");
                        Console.log("======> {}", paramMap);
                        return paramMap;
                    }
                }, null);
        System.out.println("==================== JasperReport模板渲染后的Html代码 ====================");
        Console.log(htmlList);

        // 打印参数配置
        PrintConfig printConfig = new PrintConfig(htmlList);

        System.out.println("==================== Lodop打印javascript脚本语句 ====================");
        // 生成打印控件需要的代码
        String str = new Printer(printConfig).printList();
        System.out.println(str);

        System.out.println("打印运行耗时 " + timer.intervalSecond() + " 秒");
    }

}
```

### 前端代码

引入javascript插件：

```html
<!-- html转canvas图片 -->
<script src="${staticPath}/vendors/html2canvas/html2canvas.min.js"></script>
<!-- 引入打印控件 -->
<script src="${staticPath}/vendors/lodop/LodopFuncs.js"></script>
<object  id="LODOP_OB" classid="clsid:2105C259-1E0C-4534-8141-A753534CB4CA" width=0 height=0>
       <embed id="LODOP_EM" type="application/x-print-lodop" width=0 height=0></embed>
</object>
<script src="${staticPath}/vendors/lodop/Lodop_print.js"></script>
```

调用方法：

```javascript
// html超文本内容打印
let htmlContent = $(layero).find('iframe').eq(0).contents().find('#detail').html();
printPreviewHtml(htmlContent);


// 图片打印
html2canvas($(layero).find('iframe').eq(0).contents().find('#detail').get(0)).then(canvas => {
	// 截图质量（0-1）
	printPreviewImg(canvas.toDataURL("image/png", 1), '5mm', 5, '99%', '50%');
});


// 模板渲染打印
printPreview("${adminPath}/oa/travelReimbursement/" + rowId + "/print", top.layer);
```

提供调用的javascript脚本：

```javascript
/**
 * 打印预览，用于后台返回数据打印
 *
 * @param ajaxUrl         ajax请求地址
 * @param layerAlert      layer弹窗组件对象
 */
function printPreview(ajaxUrl, layerAlert) {
    try {
        // 检查是否安装lodop
        checkIsInstall();

        let loadIndex;
        $.ajax({
            type: "POST",
            url: ajaxUrl,
            dataType: "json",
            beforeSend: function() {
                // 开启遮罩
                loadIndex = layerAlert.msg('打印模板渲染中...', {
                    icon: 16,
                    shade: 0.6,
                    time: 120000
                });
            },
            success: function(data) {
                // 关闭遮罩
                layerAlert.close(loadIndex);

                if (data.code == 500) {
                    layerAlert.alert("打印异常：" + data.msg, {icon: 2});
                    return;
                }

                // 打印
                eval(data.expression);
                LODOP.PREVIEW();
            }
        });
    } catch (e) {
        console.log(e);
    }
}

/**
 * 打印预览，用于网页内容打印
 *
 * @param htmlContent 超文本标记内容
 */
function printPreviewHtml(htmlContent) {
    try {
        checkIsInstall();
        LODOP.PRINT_INIT("网页内容打印");
        LODOP.ADD_PRINT_HTM(0, 0, "100%", "100%", htmlContent);
        LODOP.PREVIEW();
    } catch (e) {
        console.log(e);
    }
}

/**
 * 打印预览，用于图片打印
 *
 * @param base64ImgStr  图片的base64码
 * @param pageTop       上边距
 * @param pageLeft      左边距 34px(1px=1/96英寸）
 * @param imgWidth      图片宽度（默认百分百），支持数值跟百分比
 * @param imgHeight     图片高度（默认百分百），支持数值跟百分比
 * @param zooming       1：(可变形)扩展缩放模式；2：按原图比例(不变形)缩放模式；
 */
function printPreviewImg(base64ImgStr, pageTop = '5mm', pageLeft = 34, imgWidth = '100%',
                         imgHeight = '100%', zooming = 2) {
    try {
        checkIsInstall();
        LODOP.PRINT_INIT("图片打印");
        LODOP.ADD_PRINT_IMAGE(pageTop, pageLeft, imgWidth, imgHeight, base64ImgStr);
        // 按原图比例(不变形)缩放模式
        LODOP.SET_PRINT_STYLEA(0, 'Stretch', zooming);
        LODOP.PREVIEW();
    } catch (e) {
        console.log(e);
    }
}
```

## test代码执行结果

![运行示例](./doc/image/demo.png)