/**
 * =================================================
 * 注：此javascript脚本依赖jQuery、layer
 * =================================================
 */

// 声明为全局变量
var LODOP;

/**
 * 检查是否安装lodop
 */
function checkIsInstall() {
    LODOP = getLodop();
    if (LODOP.VERSION) {
        if (LODOP.CVERSION) {
            console.log("当前有WEB打印服务C-Lodop可用!");
            console.log("C-Lodop版本:", LODOP.CVERSION, "内含Lodop", LODOP.VERSION);
        } else {
            console.log("本机已成功安装了Lodop控件！版本号:", LODOP.VERSION);
        }
    }
}

/**
 * 打印预览，用于后台返回数据打印
 *
 * @param ajaxUrl         ajax请求地址
 * @param layerAlert      layer弹窗组件对象
 */
function printPreview(ajaxUrl, layerAlert) {
    try {
        // 检查是否安装lodop
        checkIsInstall();

        let loadIndex;
        $.ajax({
            type: "POST",
            url: ajaxUrl,
            dataType: "json",
            beforeSend: function() {
                // 开启遮罩
                loadIndex = layerAlert.msg('打印模板渲染中...', {
                    icon: 16,
                    shade: 0.6,
                    time: 120000
                });
            },
            success: function(data) {
                // 关闭遮罩
                layerAlert.close(loadIndex);

                if (data.code == 500) {
                    layerAlert.alert("打印异常：" + data.msg, {icon: 2});
                    return;
                }

                // 打印
                eval(data.expression);
                LODOP.PREVIEW();
            }
        });
    } catch (e) {
        console.log(e);
    }
}

/**
 * 打印预览，用于网页内容打印
 *
 * @param htmlContent 超文本标记内容
 */
function printPreviewHtml(htmlContent) {
    try {
        checkIsInstall();
        LODOP.PRINT_INIT("网页内容打印");
        LODOP.ADD_PRINT_HTM(0, 0, "100%", "100%", htmlContent);
        LODOP.PREVIEW();
    } catch (e) {
        console.log(e);
    }
}

/**
 * 打印预览，用于图片打印
 *
 * @param base64ImgStr  图片的base64码
 * @param pageTop       上边距
 * @param pageLeft      左边距 34px(1px=1/96英寸）
 * @param imgWidth      图片宽度（默认百分百），支持数值跟百分比
 * @param imgHeight     图片高度（默认百分百），支持数值跟百分比
 * @param zooming       1：(可变形)扩展缩放模式；2：按原图比例(不变形)缩放模式；
 */
function printPreviewImg(base64ImgStr, pageTop = '5mm', pageLeft = 34, imgWidth = '100%',
                         imgHeight = '100%', zooming = 2) {
    try {
        checkIsInstall();
        LODOP.PRINT_INIT("图片打印");
        LODOP.ADD_PRINT_IMAGE(pageTop, pageLeft, imgWidth, imgHeight, base64ImgStr);
        // 按原图比例(不变形)缩放模式
        LODOP.SET_PRINT_STYLEA(0, 'Stretch', zooming);
        LODOP.PREVIEW();
    } catch (e) {
        console.log(e);
    }
}