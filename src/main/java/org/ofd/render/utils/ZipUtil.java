package org.ofd.render.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;

/**
 * zip压缩工具类
 *
 * @date 2022-05-18 11:24:47
 */
@Slf4j
public class ZipUtil {

    /**
     * 打包OFD文件包二进制数据
     *
     * @param virtualFileMap 虚拟文件映射
     * @param output         输出
     * @author song_jx
     */
    public static void zip(Map<String, byte[]> virtualFileMap, OutputStream output) {
        try (ZipArchiveOutputStream zaos = new ZipArchiveOutputStream(output)) {
            for (Map.Entry<String, byte[]> entry : virtualFileMap.entrySet()) {
                zaos.putArchiveEntry(new ZipArchiveEntry(entry.getKey()));
                zaos.write(entry.getValue());
                zaos.closeArchiveEntry();
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        } finally {
            try {
                if (null != output) {
                    output.flush();
                    output.close();
                }
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }
    }

    /**
     * 打包OFD文件包二进制数据
     *
     * @param virtualFileMap 虚拟文件映射
     * @return {@link byte[] }
     * @throws IOException ioexception
     * @author song_jx
     */
    public static byte[] zip(Map<String, byte[]> virtualFileMap) throws IOException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        zip(virtualFileMap, bos);
        return bos.toByteArray();
    }

}
