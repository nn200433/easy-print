package org.ofd.render;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.date.TimeInterval;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.IoUtil;
import cn.hutool.system.SystemUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

@Slf4j
public class OFDRender {

    /**
     * 用户空间单位/毫米
     */
    private static final float POINTS_PER_MM = 1 / (10 * 2.54f) * 72;

    /**
     * 临时文件夹
     */
    private static final File TMP_DIR = new File(SystemUtil.get(SystemUtil.TMPDIR) + File.separator + "ofdFile" + File.separator);

    /**
     * ofd文件后缀
     */
    private static final String OFD_FILE_SUFFIX = ".ofd";

    /**
     * pdf转换为ofd
     *
     * @param pdfFile pdf文件
     * @return {@link File }
     * @author song_jx
     */
    public static File convertPdf2Ofd(File pdfFile) {
        return convertPdf2Ofd(IoUtil.toStream(pdfFile));
    }

    /**
     * pdf
     *
     * @param pdfInBytes pdf字节
     * @return {@link byte[] }
     * @author song_jx
     */
    public static byte[] convertPdf2Ofd(byte[] pdfInBytes) {
        byte[] b = null;
        try (PDDocument doc = PDDocument.load(IoUtil.toStream(pdfInBytes))) {
            OFDCreator ofdCreator = buildOfdCreator(doc);
            b = ofdCreator.jar();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return b;
    }

    /**
     * pdf转换为ofd
     *
     * @param pdfInputStream pdf输入流
     * @return {@link File } ofd临时文件
     * @author song_jx
     */
    public static File convertPdf2Ofd(InputStream pdfInputStream) {
        TimeInterval timer = DateUtil.timer();
        // 创建临时文件
        File tempFile = FileUtil.createTempFile("OFD-", OFD_FILE_SUFFIX, TMP_DIR, Boolean.TRUE);
        try (PDDocument doc = PDDocument.load(pdfInputStream)) {
            OFDCreator ofdCreator = buildOfdCreator(doc);
            BufferedOutputStream outputStream = FileUtil.getOutputStream(tempFile);
            ofdCreator.jar(outputStream);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        } finally {
            try {
                if (null != pdfInputStream) {
                    pdfInputStream.close();
                }
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }
        log.info("======> pdf 转 ofd 耗时 {} ms", timer.interval());
        return tempFile;
    }

    private static OFDCreator buildOfdCreator(PDDocument doc) throws IOException {
        OFDCreator ofdCreator = new OFDCreator();
        for (int i = 0; i < doc.getNumberOfPages(); i++) {
            PDPage page = doc.getPage(i);

            ofdCreator.addPage(i);
            PDRectangle cropBox = page.getCropBox();
            float widthPt = cropBox.getWidth();
            float heightPt = cropBox.getHeight();

            OFDPageDrawer ofdPageDrawer = new OFDPageDrawer(i, page, ofdCreator, 1 / POINTS_PER_MM);
            ofdPageDrawer.drawPage();
            ofdCreator.addPageContent(i, ofdPageDrawer.getCtLayer(), widthPt / POINTS_PER_MM, heightPt / POINTS_PER_MM);
        }
        return ofdCreator;
    }

}
