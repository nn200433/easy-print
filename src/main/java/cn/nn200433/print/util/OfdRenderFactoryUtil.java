package cn.nn200433.print.util;

import cn.hutool.core.io.FileUtil;
import cn.nn200433.print.config.PrintProperties;
import cn.nn200433.print.constants.PrintConstant;
import cn.nn200433.print.entity.HtmlRenderData;
import cn.nn200433.print.service.BaseRenderFactoryHandler;
import cn.nn200433.print.service.customer.JasperReportParametersCover;
import lombok.extern.slf4j.Slf4j;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import org.ofd.render.OFDRender;

import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * ofd渲染工厂工具类
 *
 * @author sjx
 * @date 2022-05-18 04:59:45
 */
@Slf4j
public class OfdRenderFactoryUtil extends BaseRenderFactoryHandler {

    public OfdRenderFactoryUtil(PrintProperties properties) {
        super(properties);
    }

    /**
     * 渲染（内部发pdf转ofd）
     *
     * @param templateName    模板名称
     * @param htmlRenderData  填充数据（若dataList为空，则采用druid数据源）
     * @param subTemplateName 子模板名称（在jasperReport中，添加相同个数名称为“SUB_REPORT_[0，1，2......]”；Class属性为“net.sf.jasperreports.engine.JasperReport”的Parameters参数）
     * @return @return {@link File }
     * @author song_jx
     */
    public static File render(String templateName, HtmlRenderData htmlRenderData, String... subTemplateName) {
        return render(templateName, htmlRenderData, PrintConstant.CUSTOMER_PARAMETERS_MAP_COVER_FUN,
                PrintConstant.CUSTOMER_DATA_LIST_COVER_FUN, subTemplateName);
    }

    /**
     * 渲染（内部发pdf转ofd）
     * 自定义数据并渲染为模板
     *
     * @param templateName           模板名称
     * @param htmlRenderData         填充数据（若dataList为空，则采用druid数据源）
     * @param customerParamMapStrFun 自定义数据对象转为JSON字符串后的处理
     * @param customerDataListStrFun 自定义数据对象转为JSON字符串后的处理
     * @param subTemplateName        子模板名称（在jasperReport中，添加相同个数名称为“SUB_REPORT_[0，1，2......]”；Class属性为“net.sf.jasperreports.engine.JasperReport”的Parameters参数）
     * @return @return {@link File }
     * @author song_jx
     */
    public static File render(String templateName,
                              HtmlRenderData htmlRenderData,
                              JasperReportParametersCover<Map<String, Object>> customerParamMapStrFun,
                              JasperReportParametersCover<List<Map<String, ?>>> customerDataListStrFun,
                              String... subTemplateName) {
        File ofdFile = null;
        File tempPdfFile = FileUtil.createTempFile("jasper", ".pdf", FileUtil.mkdir(PrintConstant.PDF_TEMP_FILE_PATH), true);
        try {
            // 导出为pdf
            JRPdfExporter jrPdfExporter = new JRPdfExporter();
            jrPdfExporter.setExporterInput(
                    new SimpleExporterInput(generateJasperPrint(templateName, htmlRenderData, customerParamMapStrFun,
                            customerDataListStrFun, subTemplateName))
            );
            jrPdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(tempPdfFile));
            jrPdfExporter.exportReport();

            // pdf 转换 ofd
            ofdFile = OFDRender.convertPdf2Ofd(tempPdfFile);
        } catch (Exception e) {
            log.error("======> 模板渲染出错，原因：", e);
        } finally {
            // 删除临时PDF
            FileUtil.del(tempPdfFile);
        }
        return ofdFile;
    }

    /**
     * 渲染（内部发pdf转ofd）
     *
     * @param templateName    模板名称
     * @param htmlRenderData  填充数据（若dataList为空，则采用druid数据源）
     * @param subTemplateName 子模板名称（在jasperReport中，添加相同个数名称为“SUB_REPORT_[0，1，2......]”；Class属性为“net.sf.jasperreports.engine.JasperReport”的Parameters参数）
     * @return @return {@link byte[] }
     * @author song_jx
     */
    public static byte[] render2Bytes(String templateName, HtmlRenderData htmlRenderData, String... subTemplateName) {
        return render2Bytes(templateName, htmlRenderData, PrintConstant.CUSTOMER_PARAMETERS_MAP_COVER_FUN,
                PrintConstant.CUSTOMER_DATA_LIST_COVER_FUN, subTemplateName);
    }

    /**
     * 渲染（内部发pdf转ofd）
     * 自定义数据并渲染为模板
     *
     * @param templateName           模板名称
     * @param htmlRenderData         填充数据（若dataList为空，则采用druid数据源）
     * @param customerParamMapStrFun 自定义数据对象转为JSON字符串后的处理
     * @param customerDataListStrFun 自定义数据对象转为JSON字符串后的处理
     * @param subTemplateName        子模板名称（在jasperReport中，添加相同个数名称为“SUB_REPORT_[0，1，2......]”；Class属性为“net.sf.jasperreports.engine.JasperReport”的Parameters参数）
     * @return @return {@link byte[] }
     * @author song_jx
     */
    public static byte[] render2Bytes(String templateName,
                                      HtmlRenderData htmlRenderData,
                                      JasperReportParametersCover<Map<String, Object>> customerParamMapStrFun,
                                      JasperReportParametersCover<List<Map<String, ?>>> customerDataListStrFun,
                                      String... subTemplateName) {
        byte[] buffer = null;
        File tempFile = null;
        try {
            tempFile = render(templateName, htmlRenderData, customerParamMapStrFun, customerDataListStrFun, subTemplateName);
            buffer = FileUtil.readBytes(tempFile);
        } catch (Exception e) {
            log.error("======> 模板渲染出错，原因：", e);
        } finally {
            FileUtil.del(tempFile);
        }
        return buffer;
    }

    /**
     * html转ofd（内部生成html转换为pdf文件，最后再转为ofd文件）
     *
     * <pre>
     * 注：默认只处理宋体
     * 页面中字体不能使用中文，需要使用英文名称，而且是大小写敏感的！例如宋体的英文名称是 SimSun(注意不是simsun！，首字母都是大写的)
     * 错误写法：font-family:宋体 或者  font-family:simsun
     * 正确写法：font-family:SimSun 或者 font-family:SimHei
     * </pre>
     *
     * @param htmlString html字符串
     * @return @return {@link File }
     * @author song_jx
     */
    public static File htmlToOfd(String htmlString) {
        return htmlToOfd(htmlString, Boolean.TRUE);
    }

    /**
     * html转ofd（内部生成html转换为pdf文件，最后再转为ofd文件）
     *
     * <pre>
     * 注：默认只处理宋体
     * 页面中字体不能使用中文，需要使用英文名称，而且是大小写敏感的！例如宋体的英文名称是 SimSun(注意不是simsun！，首字母都是大写的)
     * 错误写法：font-family:宋体 或者  font-family:simsun
     * 正确写法：font-family:SimSun 或者 font-family:SimHei
     * </pre>
     *
     * @param htmlString           html字符串
     * @param isInsideGenerateHtml 是否内部生成的Html，HtmlRenderFactoryUtil产生的html为内部生成（做过特殊处理）
     * @return @return {@link File }
     * @author song_jx
     */
    public static File htmlToOfd(String htmlString, Boolean isInsideGenerateHtml) {
        // 1. 生成pdf临时文件
        File tempPdfFile = PdfRenderFactoryUtil.htmlToPdf(htmlString, isInsideGenerateHtml);
        // 2. 生成ofd临时文件
        File ofdFile = OFDRender.convertPdf2Ofd(tempPdfFile);
        // 3. 删除pdf临时文件
        FileUtil.del(tempPdfFile);
        return ofdFile;
    }

    /**
     * html转ofd（内部生成html转换为pdf文件，最后再转为ofd文件）
     *
     * <pre>
     * 注：默认只处理宋体
     * 页面中字体不能使用中文，需要使用英文名称，而且是大小写敏感的！例如宋体的英文名称是 SimSun(注意不是simsun！，首字母都是大写的)
     * 错误写法：font-family:宋体 或者  font-family:simsun
     * 正确写法：font-family:SimSun 或者 font-family:SimHei
     * </pre>
     *
     * @param htmlString           html字符串
     * @param isInsideGenerateHtml 是否内部生成的Html，HtmlRenderFactoryUtil产生的html为内部生成（做过特殊处理）
     * @return @return {@link byte[] }
     * @author song_jx
     */
    public static byte[] htmlToOfdBytes(String htmlString, Boolean isInsideGenerateHtml) {
        byte[] buffer = null;
        File tempFile = null;
        try {
            tempFile = htmlToOfd(htmlString, isInsideGenerateHtml);
            buffer = FileUtil.readBytes(tempFile);
        } catch (Exception e) {
            log.error("======> 模板渲染出错，原因：", e);
        } finally {
            FileUtil.del(tempFile);
        }
        return buffer;
    }

    /**
     * html转ofd（内部生成html转换为pdf文件，最后再转为ofd文件）
     *
     * <pre>
     * 注：默认只处理宋体
     * 页面中字体不能使用中文，需要使用英文名称，而且是大小写敏感的！例如宋体的英文名称是 SimSun(注意不是simsun！，首字母都是大写的)
     * 错误写法：font-family:宋体 或者  font-family:simsun
     * 正确写法：font-family:SimSun 或者 font-family:SimHei
     * </pre>
     *
     * @param htmlString html字符串
     * @return @return {@link File }
     * @author song_jx
     */
    public static byte[] htmlToOfdBytes(String htmlString) {
        return htmlToOfdBytes(htmlString, Boolean.TRUE);
    }

}