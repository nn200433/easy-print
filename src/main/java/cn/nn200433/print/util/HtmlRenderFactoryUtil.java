package cn.nn200433.print.util;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.PageUtil;
import cn.hutool.core.util.ReUtil;
import cn.hutool.core.util.StrUtil;
import cn.nn200433.print.config.PrintProperties;
import cn.nn200433.print.constants.PrintConstant;
import cn.nn200433.print.entity.HtmlRenderData;
import cn.nn200433.print.service.BaseRenderFactoryHandler;
import cn.nn200433.print.service.customer.JasperReportParametersCover;
import cn.nn200433.print.service.customer.impl.DefaultJasperReportDataListCoverImpl;
import cn.nn200433.print.service.customer.impl.DefaultJasperReportParametersMapCoverImpl;
import lombok.extern.slf4j.Slf4j;
import net.sf.jasperreports.engine.export.HtmlExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleHtmlExporterOutput;
import org.beetl.core.Configuration;
import org.beetl.core.GroupTemplate;
import org.beetl.core.Template;
import org.beetl.core.resource.ClasspathResourceLoader;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * html渲染工厂
 *
 * @author sjx
 * @date 2020年05月12日 16:28:33
 */
@Slf4j
public class HtmlRenderFactoryUtil extends BaseRenderFactoryHandler {

    public HtmlRenderFactoryUtil(PrintProperties configuration) {
        super(configuration);
    }

    /**
     * 套打模式渲染
     * 自定义数据并渲染为模板
     *
     * @param templateName    模板名称
     * @param htmlRenderData  填充数据（若dataList为空，则采用druid数据源）
     * @param subTemplateName 子模板名称（在jasperReport中，添加相同个数名称为“SUB_REPORT_[0，1，2......]”；Class属性为“net.sf.jasperreports.engine.JasperReport”的Parameters参数）
     * @return {@link String}
     */
    public static List<String> renderList(String templateName, HtmlRenderData htmlRenderData, int pageDataSize,
                                          String... subTemplateName) {
        return renderList(templateName, htmlRenderData, pageDataSize, PrintConstant.CUSTOMER_PARAMETERS_MAP_COVER_FUN,
                PrintConstant.CUSTOMER_DATA_LIST_COVER_FUN, subTemplateName);
    }

    /**
     * 套打模式渲染
     * 自定义数据并渲染为模板
     *
     * @param templateName        模板名称
     * @param htmlRenderData      填充数据（若dataList为空，则采用druid数据源）
     * @param pageDataSize        每页打印数据量
     * @param customerParamMapFun 自定义每页的参数映射处理
     * @param customerDataListFun 自定义每页的数据列表处理
     * @param subTemplateName     子模板名称（在jasperReport中，添加相同个数名称为“SUB_REPORT_[0，1，2......]”；Class属性为“net.sf.jasperreports.engine.JasperReport”的Parameters参数）
     * @return {@link String}
     */
    public static List<String> renderList(String templateName, HtmlRenderData htmlRenderData, int pageDataSize,
                                          JasperReportParametersCover<Map<String, Object>> customerParamMapFun,
                                          JasperReportParametersCover<List<Map<String, ?>>> customerDataListFun,
                                          String... subTemplateName) {
        List<String> htmlList = new ArrayList<String>(0);

        try {
            if (null == htmlRenderData) {
                throw new Exception("模板渲染数据不能为空！");
            }

            Map<String, Object> parametersMap = htmlRenderData.getParamMap();
            List<Map<String, ?>> dataList = htmlRenderData.getDataList();

            if (CollectionUtil.isEmpty(dataList)) {
                return htmlList;
            }

            // 计算总数量
            int totalCount = PageUtil.totalPage(dataList.size(), pageDataSize);
            htmlList = new ArrayList<String>(totalCount);

            for (int i = 0; i < totalCount; i++) {
                int[] pageStartEndArray = PageUtil.transToStartEnd(i + 1, pageDataSize);
                HtmlRenderData tempHtmlRenderData = new HtmlRenderData(parametersMap,
                        CollUtil.sub(dataList, pageStartEndArray[0], pageStartEndArray[1]));

                // 生成单个html
                htmlList.add(render(templateName, tempHtmlRenderData, customerParamMapFun, customerDataListFun, subTemplateName));
            }
        } catch (Exception e) {
            log.error("======> 套打html生成失败，原因：", e);
        }

        return htmlList;
    }

    /**
     * 渲染（使用默认html处理程序）
     * 自定义数据并渲染为模板
     *
     * @param templateName    模板名称
     * @param htmlRenderData  填充数据（若dataList为空，则采用druid数据源）
     * @param subTemplateName 子模板名称（在jasperReport中，添加相同个数名称为“SUB_REPORT_[0，1，2......]”；Class属性为“net.sf.jasperreports.engine.JasperReport”的Parameters参数）
     * @return {@link String}
     */
    public static String render(String templateName, HtmlRenderData htmlRenderData, String... subTemplateName) {
        return render(templateName, htmlRenderData, PrintConstant.CUSTOMER_PARAMETERS_MAP_COVER_FUN,
                PrintConstant.CUSTOMER_DATA_LIST_COVER_FUN, subTemplateName);
    }

    /**
     * 渲染
     * 自定义数据并渲染为模板
     *
     * @param templateName        模板名称
     * @param htmlRenderData      填充数据（若dataList为空，则采用druid数据源）
     * @param customerParamMapFun 自定义参数映射处理。不传且 print.data.unescape 打印数据反转义启用时，使用默认实现 {@link DefaultJasperReportParametersMapCoverImpl}
     * @param customerDataListFun 自定义数据列表处理，不传且 print.data.unescape 打印数据反转义启用时，使用默认实现 {@link DefaultJasperReportDataListCoverImpl}
     * @param subTemplateName     子模板名称（在jasperReport中，添加相同个数名称为“SUB_REPORT_[0，1，2......]”；Class属性为“net.sf.jasperreports.engine.JasperReport”的Parameters参数）
     * @return {@link String}
     */
    public static String render(String templateName,
                                HtmlRenderData htmlRenderData,
                                JasperReportParametersCover<Map<String, Object>> customerParamMapFun,
                                JasperReportParametersCover<List<Map<String, ?>>> customerDataListFun,
                                String... subTemplateName) {
        // 定义最终返回结果
        StringBuffer resultRenderBuffer = new StringBuffer();
        try {

            // 导出为html
            HtmlExporter htmlExporter = new HtmlExporter();
            htmlExporter.setExporterInput(
                    new SimpleExporterInput(generateJasperPrint(templateName, htmlRenderData, customerParamMapFun,
                            customerDataListFun, subTemplateName))
            );
            htmlExporter.setExporterOutput(new SimpleHtmlExporterOutput(resultRenderBuffer));
            htmlExporter.exportReport();
        } catch (Exception e) {
            log.error("======> 模板渲染出错，原因：", e);
        }

        Document document = Jsoup.parse(resultRenderBuffer.toString());
        Elements selects = document.select(".jrPage");
        /*
        // 方法1：
        for (Element select : selects) {
            String style = selects.attr("style");
            if (StrUtil.isNotBlank(style)) {
                select.attr("style",
                        ReUtil.replaceAll(style, RE_STYLE_WIDTH, "width: 700px;"));
            }
        }
        */

        // 方法2：
        for (Element select : selects) {
            // 删除左右50%宽度标签
            String style = selects.attr("style");
            if (StrUtil.isNotBlank(style)) {
                select.attr("style",
                        ReUtil.replaceAll(style, PrintConstant.RE_STYLE_WIDTH, "width: 100%;") + "table-layout: fixed;");
            }

            // 删除tr的上下间距,让其自适应
            /*
            if (PrintConstant.PRINT_CONFIG_SETTING.getBool(PrintConstant.PRINT_CONFIG_PRINT_RENDER_HTML_TR_STYLE_REMOVE)) {
                Elements tagTrs = selects.select("tr:not(:last-child)");
                if (CollUtil.isNotEmpty(tagTrs)) {
                    for (int i = 0; i < tagTrs.size(); i++) {
                        // 过滤前三条
                        if (i < 3) {
                            continue;
                        }
                        tagTrs.get(i).removeAttr("style");
                    }
                }
            }
            */
        }

        document.selectFirst("table").selectFirst("td").remove();
        document.selectFirst("table").select("td:last-child").last().remove();

        // 单引号转义；去除换行符
        return document.html()
                .replaceAll("\'", "\\\\'")
                .replaceAll("\\n", "");
    }

    /**
     * 使用beetl渲染
     *
     * @param templateName 模板名称
     * @param dataMap      数据
     * @return {@link String }
     * @author sjx
     * @date 2020年09月30日 16:24:01
     */
    public static String render(String templateName, Map<String, Object> dataMap) {
        return render(templateName, dataMap, PrintConstant.CUSTOMER_PARAMETERS_MAP_COVER_FUN);
    }

    /**
     * 使用beetl渲染
     *
     * @param templateName       模板名称
     * @param dataMap            数据
     * @param customerDataMapFun 自定义数据对象处理
     * @return {@link String }
     * @author sjx
     * @date 2020年09月30日 16:24:01
     */
    public static String render(String templateName, Map<String, Object> dataMap,
                                JasperReportParametersCover<Map<String, Object>> customerDataMapFun) {
        String html = StrUtil.EMPTY;
        try {
            if (StrUtil.isBlank(templateName) || MapUtil.isEmpty(dataMap)) {
                throw new Exception("模板名或数据不能为空......");
            }

            // 参数不为空进行自定义操作
            if (MapUtil.isNotEmpty(dataMap) && null != customerDataMapFun) {
                dataMap = customerDataMapFun.cover(dataMap);
            }

            GroupTemplate groupTemplate = new GroupTemplate(new ClasspathResourceLoader(PrintConstant.TEMPLATE_FILE_PREFIX),
                    Configuration.defaultConfiguration());
            Template template = groupTemplate.getTemplate(templateName);
            template.binding(dataMap);
            html = template.render();
        } catch (Exception e) {
            log.error("======> beetl渲染出错", e);
        }

        // 单引号转义；去除换行符
        return html.replaceAll("\'", "\\\\'")
                .replaceAll("\\r\\n", "");
    }

}