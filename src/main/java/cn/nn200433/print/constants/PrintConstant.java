package cn.nn200433.print.constants;

import cn.hutool.core.util.StrUtil;
import cn.hutool.system.SystemUtil;
import cn.nn200433.print.service.customer.JasperReportParametersCover;
import cn.nn200433.print.service.customer.impl.DefaultJasperReportDataListCoverImpl;
import cn.nn200433.print.service.customer.impl.DefaultJasperReportParametersMapCoverImpl;

import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * 打印常量
 *
 * @Author: sjx
 * @Date: 2020年09月09日 0009 19:04:41
 */
public class PrintConstant {

    /**
     * 打印配置文件参数 - 传入打印组件的数据进行反转义
     */
    public static final String PRINT_CONFIG_PRINT_DATA_UNESCAPE = "print.data.unescape";

    /**
     * 打印配置文件参数 - html方式渲染时删除tr上的样式
     */
    public static final String PRINT_CONFIG_PRINT_RENDER_HTML_TR_STYLE_REMOVE = "print.render.html.tr.style.remove";

    /**
     * style样式中的width提取正则
     */
    public static final String RE_STYLE_WIDTH = "width:\\s?[0-9]+(px|%);";

    /**
     * 模板文件后缀
     */
    public static final String TEMPLATE_FILE_SUFFIX = ".jrxml";

    /**
     * 编译后的模板文件后缀
     */
    public static final String TEMPLATE_BUILD_FILE_SUFFIX = ".jasper";

    /**
     * 模板文件相对于resource下的前缀
     */
    public static final String TEMPLATE_FILE_PREFIX = "print-template" + File.separator;

    /**
     * windows字体文件的路径前缀
     */
    public static final String WINDOWS_FONT_FILE_PREFIX = StrUtil.subBefore(SystemUtil.get("user.home"), File.separator, false) + "\\Windows\\Fonts\\";

    /**
     * windows字体文件的路径前缀
     */
    public static final String LINUX_FONT_FILE_PREFIX = "/usr/share/fonts/";

    /**
     * pdf临时文件路径
     */
    public static final String PDF_TEMP_FILE_PATH = SystemUtil.get(SystemUtil.TMPDIR) + File.separator + "jrPdfExport" + File.separator;

    /**
     * 打印页面为横向
     */
    public static final Integer PRINT_PAGE_DIRECTION_HORIZONTAL = 2;

    /**
     * parametersMap 的默认 JasperReportParametersCover实现
     */
    public static final JasperReportParametersCover<Map<String, Object>> CUSTOMER_PARAMETERS_MAP_COVER_FUN = new DefaultJasperReportParametersMapCoverImpl();

    /**
     * dataList 的默认 JasperReportParametersCover实现
     */
    public static final JasperReportParametersCover<List<Map<String, ?>>> CUSTOMER_DATA_LIST_COVER_FUN = new DefaultJasperReportDataListCoverImpl();

}