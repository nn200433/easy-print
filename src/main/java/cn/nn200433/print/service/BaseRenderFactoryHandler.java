package cn.nn200433.print.service;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.io.resource.ResourceUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.StrUtil;
import cn.nn200433.print.config.PrintProperties;
import cn.nn200433.print.constants.PrintConstant;
import cn.nn200433.print.entity.HtmlRenderData;
import cn.nn200433.print.service.customer.JasperReportParametersCover;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRMapCollectionDataSource;

import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 基础打印渲染工具类
 *
 * @Author: sjx
 * @Date: 2020年09月10日 0010 07:41:34
 */
public class BaseRenderFactoryHandler {

    protected static PrintProperties printProperties;

    public BaseRenderFactoryHandler(PrintProperties properties) {
        printProperties = properties;
    }

    /**
     * 创建JasperPrint对象
     *
     * @param templateName           模板名称
     * @param htmlRenderData         填充数据（若dataList为空，则采用druid数据源）
     * @param customerParamMapStrFun 自定义数据对象转为JSON字符串后的处理
     * @param customerDataListStrFun 自定义数据对象转为JSON字符串后的处理
     * @param subTemplateName        子模板名称（在jasperReport中，添加相同个数名称为“SUB_REPORT_[0，1，2......]”；Class属性为“net.sf.jasperreports.engine.JasperReport”的Parameters参数）
     * @return {@link JasperPrint }
     * @throws Exception 异常
     */
    protected static JasperPrint generateJasperPrint(String templateName,
                                                     HtmlRenderData htmlRenderData,
                                                     JasperReportParametersCover<Map<String, Object>> customerParamMapStrFun,
                                                     JasperReportParametersCover<List<Map<String, ?>>> customerDataListStrFun,
                                                     String... subTemplateName) throws Exception {
        if (null == htmlRenderData) {
            throw new Exception("模板渲染数据不能为空！");
        }

        Map<String, Object> parametersMap = htmlRenderData.getParamMap();
        List<Map<String, ?>> dataList = htmlRenderData.getDataList();

        // region 数据不为空，进行数据处理
        if (MapUtil.isNotEmpty(parametersMap) && printProperties.isDataUnescape()) {
            if (null != customerParamMapStrFun) {
                parametersMap = customerParamMapStrFun.cover(parametersMap);
            }
        }

        if (CollUtil.isNotEmpty(dataList) && printProperties.isDataUnescape()) {
            if (null != customerDataListStrFun) {
                dataList = customerDataListStrFun.cover(dataList);
            }
        }
        // endregion

        // 编译模板
        JasperReport jasperReport = compileTemplate(parametersMap, templateName, subTemplateName);

        // 填充模板内容
        JasperPrint jasperPrint = fillInTemplate(htmlRenderData, jasperReport, parametersMap, dataList);

        return jasperPrint;
    }

    /**
     * 编译模板
     *
     * @param parametersMap   模板参数映射
     * @param templateName    主模板名称
     * @param subTemplateName 子模板名称，可多个
     * @return @return {@link JasperReport }
     * @throws Exception 异常
     * @author song_jx
     */
    private static JasperReport compileTemplate(Map<String, Object> parametersMap,
                                                String templateName,
                                                String... subTemplateName) throws Exception {
        String templatePath = null;
        if (StrUtil.isNotBlank(printProperties.getTemplatePath())) {
            templatePath = printProperties.getTemplatePath();
            if (!StrUtil.endWith(templatePath, StrUtil.SLASH)) {
                templatePath += File.separator;
            }
        }

        // 1. 获取jasperReport主模板，并编译
        InputStream jasperStream = ResourceUtil.getStream(StrUtil.blankToDefault(templatePath, PrintConstant.TEMPLATE_FILE_PREFIX) +
                templateName + PrintConstant.TEMPLATE_FILE_SUFFIX);
        JasperReport jasperReport = JasperCompileManager.compileReport(jasperStream);
        jasperStream.close();

        // 1.1 获取子模板数据
        if (ArrayUtil.isNotEmpty(subTemplateName)) {
            for (int i = 0; i < subTemplateName.length; i++) {
                /*
                // 方法1：直接使用编译好的文件
                InputStream subReportInputStream = ResourceUtil.getStream(TEMPLATE_FILE_PREFIX + subTemplateName[i] + TEMPLATE_BUILD_FILE_SUFFIX);
                JasperReport subReport = (JasperReport) JRLoader.loadObject(subReportInputStream);
                */
                InputStream subReportInputStream = ResourceUtil.getStream(StrUtil.blankToDefault(templatePath, PrintConstant.TEMPLATE_FILE_PREFIX) +
                        subTemplateName[i] + PrintConstant.TEMPLATE_FILE_SUFFIX);
                JasperReport subReport = JasperCompileManager.compileReport(subReportInputStream);
                parametersMap.put("SUB_REPORT_" + i, subReport);
                subReportInputStream.close();
            }
        }
        return jasperReport;
    }

    /**
     * 填充模板内容
     *
     * @param htmlRenderData html呈现数据
     * @param jasperReport   主报表模板流
     * @param parametersMap  模板参数映射
     * @param dataList       模板可循环数据列表
     * @return @return {@link JasperPrint }
     * @throws Exception 异常
     * @author song_jx
     */
    private static JasperPrint fillInTemplate(HtmlRenderData htmlRenderData,
                                              JasperReport jasperReport,
                                              Map<String, Object> parametersMap,
                                              List<Map<String, ?>> dataList) throws Exception {
        JasperPrint jasperPrint = null;
        switch (htmlRenderData.getMode()) {
            case DATA_FROM_DB:
                throw new Exception("======> 数据库连接方式打印暂无法使用...");
                /*
                // 获取druid数据源连接
                DruidDataSource dataSource = SpringUtil.getBean("dataSource", DruidDataSource.class);
                if (null == dataSource) {
                    throw new Exception("dataSource获取失败！");
                }
                DruidPooledConnection connection = dataSource.getConnection();

                // 填充数据
                jasperPrint = JasperFillManager.fillReport(jasperReport,
                        MapUtil.defaultIfEmpty(parametersMap, new HashMap<String, Object>(0)),
                        connection);

                if (null != connection) {
                    connection.close();
                }
                */
            case DATA_FROM_COLLECTION_MAP:

                // 用传入的 List<Map<String, ?> 构造数据源
                JRDataSource jrDataSource = null;
                if (CollectionUtil.isEmpty(dataList)) {
                    // dataList为空，传入空数据源
                    jrDataSource = new JREmptyDataSource();
                } else {
                    jrDataSource = new JRMapCollectionDataSource(dataList);
                }

                // 填充数据
                jasperPrint = JasperFillManager.fillReport(jasperReport,
                        MapUtil.defaultIfEmpty(parametersMap, new HashMap<String, Object>(0)),
                        jrDataSource);

                break;
            case DATA_FROM_EMPTY:
            default:

                // 填充数据
                jasperPrint = JasperFillManager.fillReport(jasperReport,
                        MapUtil.defaultIfEmpty(parametersMap, new HashMap<String, Object>(0)),
                        new JREmptyDataSource());
        }
        return jasperPrint;
    }

}