package cn.nn200433.print.service.customer.impl;

import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HtmlUtil;
import cn.nn200433.print.service.customer.JasperReportParametersCover;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;

import java.util.List;
import java.util.Map;

/**
 * dataList 的默认 JasperReportParametersCover实现
 *
 * @author song_jx
 * @date 2021-08-13 03:32:06
 */
public class DefaultJasperReportDataListCoverImpl implements JasperReportParametersCover<List<Map<String, ?>>> {

    @Override
    public List<Map<String, ?>> cover(List<Map<String, ?>> dataList) throws Exception {
        String dataListStr = HtmlUtil.unescape(JSON.toJSONString(dataList))
                .replace("<br>", StrUtil.CRLF)
                .replace("<br/>", StrUtil.CRLF);
        return JSON.parseObject(dataListStr, new TypeReference<List<Map<String, ?>>>() {
        }.getType());
    }

}