package cn.nn200433.print.service.customer.impl;

import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HtmlUtil;
import cn.nn200433.print.service.customer.JasperReportParametersCover;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;

import java.util.Map;

/**
 * parametersMap 的默认 JasperReportParametersCover实现
 *
 * @author song_jx
 * @date 2021-08-13 03:31:50
 */
public class DefaultJasperReportParametersMapCoverImpl implements JasperReportParametersCover<Map<String, Object>> {

    @Override
    public Map<String, Object> cover(Map<String, Object> parametersMap) throws Exception {
        String parametersMapStr = HtmlUtil.unescape(JSON.toJSONString(parametersMap))
                .replace("<br>", StrUtil.CRLF)
                .replace("<br/>", StrUtil.CRLF);
        return JSON.parseObject(parametersMapStr, new TypeReference<Map<String, Object>>() {
        }.getType());
    }

}