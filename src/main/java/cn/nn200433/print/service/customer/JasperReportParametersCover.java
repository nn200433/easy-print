package cn.nn200433.print.service.customer;

/**
 * jasperReport报表参数覆盖
 *
 * @author sjx
 * @date 2020年10月12日 19:00:29
 */
public interface JasperReportParametersCover<T> {

    /**
     * 覆盖
     *
     * @param t 数据
     * @return @return {@link T }
     * @throws Exception 异常
     * @author song_jx
     */
    public T cover(T t) throws Exception;

}
