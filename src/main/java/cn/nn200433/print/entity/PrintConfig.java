package cn.nn200433.print.entity;

import cn.hutool.core.util.StrUtil;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * 打印纸张配置
 *
 * @author sjx
 * @date 2020年05月12日 15:28:39
 */
@Getter
@Setter
public class PrintConfig {

    /**
     * 打印任务名
     */
    private String taskName = "打印任务";

    /**
     * 纸张名字，如A4
     */
    private String pageSizeName = PaperStyle.DEFAULT.getName();

    /**
     * 纸张宽度
     */
    private int pageSizeWidth = PaperStyle.DEFAULT.getWidth();

    /**
     * 纸张高度
     */
    private int pageSizeHeight = PaperStyle.DEFAULT.getHeight();

    /**
     * 打印内容左上角与纸张的顶部距离
     */
    private Object htmlTop = 0;

    /**
     * 打印内容左上角与纸张左侧距离
     */
    private Object htmlLeft = 0;

    /**
     * 打印内容宽度
     */
    private Object htmlWith = "100%";

    /**
     * 打印内容高度
     */
    private Object htmlHeight = "100%";

    /**
     * <ul>
     *     <li>0 - 方向不定，由操作者自行选择或按打印机缺省设置；</li>
     *     <li>1 - 纵向打印，固定纸张；</li>
     *     <li>2 - 横向打印，固定纸张；</li>
     *     <li>3 - 纵向打印，宽度固定，高度按打印内容的高度自适应；</li>
     * </ul>
     */
    private int direction = 0;

    /**
     * 用户自定义添加内容
     * <p>
     *     例如：
     *     LODOP.SET_SHOW_MODE("HIDE_PBUTTIN_PREVIEW",1);//隐藏打印按钮
     *     LODOP.SET_SHOW_MODE("HIDE_SBUTTIN_PREVIEW",1);//隐藏设置按钮
     * </p>
     */
    private String customerAddContent = StrUtil.EMPTY;

    /**
     * 待打印的html内容
     */
    private String printHtml = "";

    /**
     * 待打印的html内容集合
     */
    private List<String> printHtmlList = new ArrayList<String>(0);

    public PrintConfig() {
    }

    /**
     * 使用默认样式打印
     *
     * @param printHtml
     */
    public PrintConfig(String printHtml) {
        this.printHtml = printHtml;
    }

    /**
     * 使用默认样式打印
     *
     * @param printHtmlList
     */
    public PrintConfig(List<String> printHtmlList) {
        this.printHtmlList = printHtmlList;
    }

    /**
     * 打印配置
     *
     * @param printHtml  待打印的html
     * @param paperStyle 纸张风格，如A4、A3
     */
    public PrintConfig(String printHtml, PaperStyle paperStyle) {
        this.printHtml = printHtml;

        // 纸张样式
        this.pageSizeName = paperStyle.getName();
        this.pageSizeWidth = paperStyle.getWidth();
        this.pageSizeHeight = paperStyle.getHeight();
    }

}