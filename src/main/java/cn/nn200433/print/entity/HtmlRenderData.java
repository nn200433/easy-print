package cn.nn200433.print.entity;

import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Map;

/**
 * html渲染需要的数据
 *
 * @author sjx
 * @date 2020年05月12日 0012 16:42:32
 */
public class HtmlRenderData {

    /**
     * 数据来源
     * <p>
     * 0 - 无需数据源
     * 1 - dataList
     * 2 - druid
     */
    @Getter
    private HtmlRenderDataSource mode = HtmlRenderDataSource.DATA_FROM_EMPTY;

    /**
     * 报表中的parameters数据，该数据一般为 标题、申请人、时间......信息
     */
    @Setter
    @Getter
    private Map<String, Object> paramMap;

    /**
     * 报表中的数据源，一般指要循环输出部分的数据</br>
     * 数据源为数据库时，可不传
     */
    @Setter
    @Getter
    private List<Map<String, ?>> dataList;

    public HtmlRenderData() {
    }

    /**
     * 构造函数
     *
     * @param paramMap 报表中的parameters数据，该数据一般为 标题、申请人、时间......信息
     * @return {@link  }
     */
    public HtmlRenderData(Map<String, Object> paramMap) {
        this.paramMap = paramMap;
        this.mode = HtmlRenderDataSource.DATA_FROM_DB;
    }

    /**
     * 构造函数
     *
     * @param paramMap 报表中的parameters数据，该数据一般为 标题、申请人、时间......信息
     * @param mode     数据来源
     * @return {@link  }
     */
    public HtmlRenderData(Map<String, Object> paramMap, HtmlRenderDataSource mode) {
        this.paramMap = paramMap;
        this.mode = mode;
    }

    /**
     * 构造函数
     *
     * @param paramMap 报表中的parameters数据，该数据一般为 标题、申请人、时间......信息
     * @param dataList 报表中的数据源，一般指要循环输出部分的数据
     * @return {@link  }
     */
    public HtmlRenderData(Map<String, Object> paramMap, List<Map<String, ?>> dataList) {
        this.paramMap = paramMap;
        this.dataList = dataList;
        this.mode = HtmlRenderDataSource.DATA_FROM_COLLECTION_MAP;
    }

}