package cn.nn200433.print.entity;

import lombok.Getter;

/**
 * jasperReports数据获取来源
 *
 * @author sjx
 * @date 2020年05月12日 0012 15:44:58
 */
@Getter
public enum HtmlRenderDataSource {

    /**
     * 无数据源
     */
    DATA_FROM_EMPTY(0),

    /**
     * Collection<Map<String,?>>
     */
    DATA_FROM_COLLECTION_MAP(1),

    /**
     * 数据库连接
     */
    DATA_FROM_DB(99);

    /**
     * 模式
     */
    private int dataSourceMode;

    private HtmlRenderDataSource(int dataSourceMode) {
        this.dataSourceMode = dataSourceMode;
    }

}