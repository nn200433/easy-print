package cn.nn200433.print.entity;

import lombok.Getter;

/**
 * 各个尺寸纸张样式
 * <p>
 * 注意：这里的长度是 毫米 x 10 后的结果
 *
 * @author sjx
 * @date 2020年05月12日 0012 15:44:58
 */
@Getter
public enum PaperStyle {

    /**
     * 默认选项
     */
    DEFAULT("A4", 0, 0),

    /**
     * A0纸尺寸：841×1189；
     */
    A0("A0", 8410, 1189),

    /**
     * A1纸尺寸：594×841；
     */
    A1("A1", 5940, 8410),

    /**
     * A2纸尺寸：420×594；
     */
    A2("A2", 4200, 5940),

    /**
     * A3纸尺寸：297×420；
     */
    A3("A3", 2970, 4200),

    /**
     * A4纸尺寸：210×297；
     */
    A4("A4", 2100, 2970);

    private String name;

    private int width;

    private int height;

    private PaperStyle(String name, int width, int height) {
        this.name = name;
        this.width = width;
        this.height = height;
    }

}