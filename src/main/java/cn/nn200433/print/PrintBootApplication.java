package cn.nn200433.print;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;


@SpringBootApplication
@Import(cn.hutool.extra.spring.SpringUtil.class)
public class PrintBootApplication {

    public static void main(String[] args) {
        SpringApplication.run(PrintBootApplication.class, args);
    }

}
