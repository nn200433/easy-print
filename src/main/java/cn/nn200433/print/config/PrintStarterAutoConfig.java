package cn.nn200433.print.config;

import cn.nn200433.print.util.HtmlRenderFactoryUtil;
import cn.nn200433.print.util.OfdRenderFactoryUtil;
import cn.nn200433.print.util.PdfRenderFactoryUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 打印配置
 *
 * @author song_jx
 * @date 2021年12月14日 0014 19:49:36
 */
@Configuration
@EnableConfigurationProperties(value = PrintProperties.class)
public class PrintStarterAutoConfig {

    @Autowired
    private PrintProperties printProperties;

    @Bean
    @ConditionalOnMissingBean
    public HtmlRenderFactoryUtil htmlRenderFactoryUtil() {
        return new HtmlRenderFactoryUtil(printProperties);
    }

    @Bean
    @ConditionalOnMissingBean
    public PdfRenderFactoryUtil pdfRenderFactoryUtil() {
        return new PdfRenderFactoryUtil(printProperties);
    }

    @Bean
    @ConditionalOnMissingBean
    public OfdRenderFactoryUtil ofdRenderFactoryUtil() {
        return new OfdRenderFactoryUtil(printProperties);
    }

}
