package cn.nn200433.print.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 打印配置
 *
 * @author song_jx
 * @date 2021年12月14日 0014 19:48:59
 */
@Getter
@Setter
@ConfigurationProperties(prefix = "print")
public class PrintProperties {

    /**
     * 传入打印组件的数据进行反转义
     */
    private boolean dataUnescape;

    /**
     * 模板路径
     */
    private String templatePath;

}
