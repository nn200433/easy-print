package cn.nn200433.print;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.StrUtil;
import cn.nn200433.print.constants.PrintConstant;
import cn.nn200433.print.entity.PrintConfig;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * 调用打印中间件方法
 *
 * @author sjx
 * @date 2020年05月12日 0012 16:22:15
 */
public class Printer {

    /**
     * 打印设置
     */
    @Setter
    @Getter
    private PrintConfig printConfig = new PrintConfig();

    public Printer() {
    }

    public Printer(PrintConfig printConfig) {
        this.printConfig = printConfig;
    }

    /**
     * 打印
     *
     * @return {@link String} html
     * @throws Exception 异常
     */
    public String print() throws Exception {
        StringBuffer fltContent = new StringBuffer();


        // 打印任务名
        fltContent.append(StrUtil.format("LODOP.PRINT_INIT('{}');", printConfig.getTaskName()));

        // 打印后自动关闭预览页面
        fltContent.append("LODOP.SET_PRINT_MODE('AUTO_CLOSE_PREWINDOW', 1);");

        // 渲染模板
        fltContent.append("LODOP.ADD_PRINT_HTM(")
                // top
                .append(chooseAppend(printConfig.getHtmlTop(), false))
                // left
                .append(chooseAppend(printConfig.getHtmlLeft(), true))
                // width
                .append(chooseAppend(printConfig.getHtmlWith(), true))
                // height
                .append(chooseAppend(printConfig.getHtmlHeight(), true))
                // 打印内容
                .append(chooseAppend(printConfig.getPrintHtml(), true))
                .append(");");

        fltContent.append(StrUtil.format("LODOP.SET_PRINT_PAGESIZE({}, {}, {}, '{}');",
                printConfig.getDirection(), printConfig.getPageSizeWidth(), printConfig.getPageSizeHeight(),
                printConfig.getPageSizeName()));

        // 横向打印时，正向显示
        if (printConfig.getDirection() == PrintConstant.PRINT_PAGE_DIRECTION_HORIZONTAL) {
            fltContent.append("LODOP.SET_SHOW_MODE('LANDSCAPE_DEFROTATED',1);");
        }

        // 用户自定义指令
        fltContent.append(printConfig.getCustomerAddContent());

        return fltContent.toString();
    }

    /**
     * 套打
     *
     * @return {@link String} html
     * @throws Exception 异常
     */
    public String printList() throws Exception {
        StringBuffer fltContent = new StringBuffer();

        List<String> printHtmlList = printConfig.getPrintHtmlList();
        if (CollUtil.isEmpty(printHtmlList) || printHtmlList.size() <= 1) {
            throw new Exception("参数【printHtmlList】数据为空或者数据量小于1条......");
        }

        // 打印任务名
        fltContent.append(StrUtil.format("LODOP.PRINT_INIT('{}');", printConfig.getTaskName()));

        // 打印后自动关闭预览页面
        fltContent.append("LODOP.SET_PRINT_MODE('AUTO_CLOSE_PREWINDOW', 1);");

        // 渲染模板
        for (int i = 0; i < printHtmlList.size(); i++) {
            fltContent.append("LODOP.ADD_PRINT_HTM(")
                    // top
                    .append(chooseAppend(printConfig.getHtmlTop(), false))
                    // left
                    .append(chooseAppend(printConfig.getHtmlLeft(), true))
                    // width
                    .append(chooseAppend(printConfig.getHtmlWith(), true))
                    // height
                    .append(chooseAppend(printConfig.getHtmlHeight(), true))
                    // 打印内容
                    .append(chooseAppend(printHtmlList.get(i), true))
                    .append(");");

            // 强制分页
            if (i != (printHtmlList.size() - 1)) {
                fltContent.append("LODOP.NewPageA();");
            }
        }

        fltContent.append(StrUtil.format("LODOP.SET_PRINT_PAGESIZE({}, {}, {}, '{}');",
                printConfig.getDirection(), printConfig.getPageSizeWidth(), printConfig.getPageSizeHeight(),
                printConfig.getPageSizeName()));

        // 横向打印时，正向显示
        if (printConfig.getDirection() == PrintConstant.PRINT_PAGE_DIRECTION_HORIZONTAL) {
            fltContent.append("LODOP.SET_SHOW_MODE('LANDSCAPE_DEFROTATED',1);");
        }

        // 用户自定义指令
        fltContent.append(printConfig.getCustomerAddContent());

        return fltContent.toString();
    }

    /**
     * 选择添加
     *
     * @param data       数据
     * @param isHasComma 有逗号
     * @return {@link String}
     */
    private String chooseAppend(Object data, boolean isHasComma) {
        if (isHasComma) {
            if (data instanceof String) {
                return ",'" + Convert.toStr(data) + "'";
            } else {
                return "," + Convert.toStr(data);
            }
        } else {
            if (data instanceof String) {
                return "'" + Convert.toStr(data) + "'";
            } else {
                return Convert.toStr(data);
            }
        }
    }

}