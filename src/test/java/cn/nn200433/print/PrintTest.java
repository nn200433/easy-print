package cn.nn200433.print;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.date.TimeInterval;
import cn.hutool.core.lang.Console;
import cn.nn200433.print.entity.HtmlRenderData;
import cn.nn200433.print.entity.PrintConfig;
import cn.nn200433.print.service.customer.JasperReportParametersCover;
import cn.nn200433.print.util.HtmlRenderFactoryUtil;
import cn.nn200433.print.util.OfdRenderFactoryUtil;
import cn.nn200433.print.util.PdfRenderFactoryUtil;
import com.forte.util.utils.MockUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 打印测试
 *
 * @author sjx
 * @date 2020年05月18日 0018 15:05:05
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class PrintTest {

    // region ============================= pdf测试 =============================

    /**
     * 示例1 - jasperReport导出html使用示例
     *
     * @throws Exception 异常
     */
    @Test
    public void demo1() throws Exception {
        TimeInterval timer = DateUtil.timer();

        Map<String, Object> paramMaps = new HashMap<String, Object>(1);
        paramMaps.put("TABLE_TITLE", "测试标题");

        List<Map<String, ?>> detailList = new ArrayList<Map<String, ?>>(20);
        for (int i = 0; i < 20; i++) {
            Map<String, Object> tempMap = new HashMap<String, Object>();
            tempMap.put("FIELD_ACCOUNT", MockUtil.title(5));
            tempMap.put("FIELD_REAL_NAME", MockUtil.cname());
            detailList.add(tempMap);
        }

        // 模板渲染
        String html = HtmlRenderFactoryUtil.render("Test_A4",
                new HtmlRenderData(paramMaps, detailList), new JasperReportParametersCover<Map<String, Object>>() {
                    @Override
                    public Map<String, Object> cover(Map<String, Object> parameterMap) throws Exception {
                        return parameterMap;
                    }
                }, new JasperReportParametersCover<List<Map<String, ?>>>() {
                    @Override
                    public List<Map<String, ?>> cover(List<Map<String, ?>> dataList) throws Exception {
                        return dataList;
                    }
                });

        System.out.println("==================== JasperReport模板渲染后的Html代码 ====================");
        System.out.println(html);

        // 打印参数配置
        PrintConfig printConfig = new PrintConfig(html);

        // 生成打印控件需要的代码
        String str = new Printer(printConfig).print();

        System.out.println("==================== Lodop打印javascript脚本语句 ====================");
        System.out.println(str);

        System.out.println("打印运行耗时 " + timer.intervalSecond() + " 秒");
    }

    /**
     * 示例2 - jasperReport导出pdf示例
     *
     * @return
     * @author song_jx
     */
    @Test
    public void demo2() {
        TimeInterval timer = DateUtil.timer();

        Map<String, Object> paramMaps = new HashMap<String, Object>(1);
        paramMaps.put("TABLE_TITLE", "测试&bull;标题");

        List<Map<String, ?>> detailList = new ArrayList<Map<String, ?>>(20);
        for (int i = 0; i < 20; i++) {
            Map<String, Object> tempMap = new HashMap<String, Object>();
            tempMap.put("FIELD_ACCOUNT", MockUtil.title(5) + "&gt;");
            tempMap.put("FIELD_REAL_NAME", MockUtil.cname());
            detailList.add(tempMap);
        }

        // 模板渲染
        File pdfFile = PdfRenderFactoryUtil.render("Test_A4",
                new HtmlRenderData(paramMaps, detailList));

        Console.log("======> 文件路径 = {}", pdfFile.getAbsolutePath());

        System.out.println("打印运行耗时 " + timer.intervalSecond() + " 秒");
    }

    /**
     * 示例3 - html转pdf
     *
     * @return
     * @author song_jx
     */
    @Test
    public void demo3() {
        // 字体只处理宋体！！！！

        String s = "<!DOCTYPE html PUBLIC\"-//W3C//DTD XHTML 1.0 Transitional//EN\"\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\"><html><head><title></title><meta http-equiv=\"Content-Type\"content=\"text/html; charset=UTF-8\"/><style type=\"text/css\">a{text-decoration:none}</style></head><body text=\"#000000\"link=\"#000000\"alink=\"#000000\"vlink=\"#000000\"><table width=\"100%\"cellpadding=\"0\"cellspacing=\"0\"border=\"0\"><tr><td width=\"50%\">&nbsp;</td><td align=\"center\"><a name=\"JR_PAGE_ANCHOR_0_1\"></a><table class=\"jrPage\"cellpadding=\"0\"cellspacing=\"0\"border=\"0\"style=\"empty-cells: show; width: 595px; border-collapse: collapse; background-color: white;\"><tr valign=\"top\"style=\"height:0\"><td style=\"width:20px\"></td><td style=\"width:180px\"></td><td style=\"width:100px\"></td><td style=\"width:100px\"></td><td style=\"width:175px\"></td><td style=\"width:20px\"></td></tr><tr valign=\"top\"style=\"height:20px\"><td colspan=\"6\"></td></tr><tr valign=\"top\"style=\"height:40px\"><td colspan=\"2\"></td><td colspan=\"2\"style=\"text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 21px; line-height: 1.140625;\">测试标题</span></td><td colspan=\"2\"></td></tr><tr valign=\"top\"style=\"height:20px\"><td colspan=\"6\"></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625; font-weight: bold;\">帐号</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625; font-weight: bold;\">真实姓名</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">Vtvjo</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">席皂斋</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">Ltoix</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">连雪空</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">Jazsj</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">屈速</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">Tzans</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">谷梁成</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">Eqakb</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">寿呈</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">Izgmw</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">萧菜铣</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">Vizmk</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">隆噶</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">Nhbkv</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">宦譬</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">Hhpzy</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">郇欠放</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">Xyupx</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">蔺换稍</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">Usczd</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">祖玩该</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">Wkfkc</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">韶芥写</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">Qgrvq</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">綦毋恋</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">Dmkmb</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">鲁驯</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">Yzlsc</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">籍仿</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">Nfwkd</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">都筒</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">Nsegj</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">莘喇距</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">Mvnln</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">覃知</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">Tkbkb</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">谈酸</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">Zynak</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">边捷</span></td><td></td></tr><tr valign=\"top\"style=\"height:132px\"><td colspan=\"6\"></td></tr></table></td><td width=\"50%\">&nbsp;</td></tr></table></body></html>";
        File file = PdfRenderFactoryUtil.htmlToPdf(s, Boolean.FALSE);
        Console.log("======> 文件路径1 = {}", file.getAbsolutePath());
        // 该 html 的 style中的 font-family: \'宋体\' 经过特殊处理，需注意
        String s1 = "<!DOCTYPE html PUBLIC\"-//W3C//DTD XHTML 1.0 Transitional//EN\"\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\"><html><head><title></title><meta http-equiv=\"Content-Type\"content=\"text/html; charset=UTF-8\"/><style type=\"text/css\">a{text-decoration:none}</style></head><body text=\"#000000\"link=\"#000000\"alink=\"#000000\"vlink=\"#000000\"><table width=\"100%\"cellpadding=\"0\"cellspacing=\"0\"border=\"0\"><tr><td width=\"50%\">&nbsp;</td><td align=\"center\"><a name=\"JR_PAGE_ANCHOR_0_1\"></a><table class=\"jrPage\"cellpadding=\"0\"cellspacing=\"0\"border=\"0\"style=\"empty-cells: show; width: 595px; border-collapse: collapse; background-color: white;\"><tr valign=\"top\"style=\"height:0\"><td style=\"width:20px\"></td><td style=\"width:180px\"></td><td style=\"width:100px\"></td><td style=\"width:100px\"></td><td style=\"width:175px\"></td><td style=\"width:20px\"></td></tr><tr valign=\"top\"style=\"height:20px\"><td colspan=\"6\"></td></tr><tr valign=\"top\"style=\"height:40px\"><td colspan=\"2\"></td><td colspan=\"2\"style=\"text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 21px; line-height: 1.140625;\">测试标题</span></td><td colspan=\"2\"></td></tr><tr valign=\"top\"style=\"height:20px\"><td colspan=\"6\"></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625; font-weight: bold;\">帐号</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625; font-weight: bold;\">真实姓名</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">Vtvjo</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">席皂斋</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">Ltoix</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">连雪空</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">Jazsj</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">屈速</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">Tzans</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">谷梁成</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">Eqakb</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">寿呈</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">Izgmw</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">萧菜铣</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">Vizmk</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">隆噶</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">Nhbkv</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">宦譬</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">Hhpzy</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">郇欠放</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">Xyupx</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">蔺换稍</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">Usczd</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">祖玩该</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">Wkfkc</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">韶芥写</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">Qgrvq</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">綦毋恋</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">Dmkmb</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">鲁驯</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">Yzlsc</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">籍仿</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">Nfwkd</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">都筒</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">Nsegj</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">莘喇距</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">Mvnln</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: '\\'宋体\\''; color: #000000; font-size: 18px; line-height: 1.140625;\">覃知</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">Tkbkb</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">谈酸</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">Zynak</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">边捷</span></td><td></td></tr><tr valign=\"top\"style=\"height:132px\"><td colspan=\"6\"></td></tr></table></td><td width=\"50%\">&nbsp;</td></tr></table></body></html>";
        File file1 = PdfRenderFactoryUtil.htmlToPdf(s1);
        Console.log("======> 文件路径2 = {}", file1.getAbsolutePath());
    }

    /**
     * 示例4 - 使用beetl模板引擎
     *
     * @throws Exception 异常
     */
    @Test
    public void demo4() throws Exception {
        TimeInterval timer = DateUtil.timer();

        Map<String, Object> paramMaps = new HashMap<String, Object>(1);
        paramMaps.put("title", "测试标题");
        paramMaps.put("date", "2020-01-01 至 2020-02-28");

        List<Map<String, Object>> dataList = new ArrayList<Map<String, Object>>(3);
        for (int i = 0; i < 5; i++) {
            HashMap<String, Object> stringObjectHashMap = new HashMap<>();
            stringObjectHashMap.put("orgPosition", "测试" + i);
            stringObjectHashMap.put("opinion", "测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试");
            stringObjectHashMap.put("createByName", "姓名_" + i);
            stringObjectHashMap.put("createDateStr", "2020-09-30");
            dataList.add(stringObjectHashMap);
        }
        paramMaps.put("dataList", dataList);
        // 模板渲染
        String html = HtmlRenderFactoryUtil.render("demo4.html", paramMaps);

        System.out.println("==================== beetl模板渲染后的Html代码 ====================");
        System.out.println(html);

        // 打印参数配置
        PrintConfig printConfig = new PrintConfig(html);

        // 生成打印控件需要的代码
        String str = new Printer(printConfig).print();

        System.out.println("==================== Lodop打印javascript脚本语句 ====================");
        System.out.println(str);

        System.out.println("打印运行耗时 " + timer.intervalSecond() + " 秒");
    }

    /**
     * 示例5 - 套打
     *
     * @throws Exception 异常
     */
    @Test
    public void demo5() throws Exception {
        TimeInterval timer = DateUtil.timer();

        Map<String, Object> paramMaps = new HashMap<String, Object>(1);
        paramMaps.put("TABLE_TITLE", "测试标题");

        List<Map<String, ?>> detailList = new ArrayList<Map<String, ?>>(20);
        for (int i = 0; i < 5; i++) {
            Map<String, Object> tempMap = new HashMap<String, Object>();
            tempMap.put("FIELD_ACCOUNT", MockUtil.title(5));
            tempMap.put("FIELD_REAL_NAME", MockUtil.cname());
            detailList.add(tempMap);
        }

        // 模板渲染
        List<String> htmlList = HtmlRenderFactoryUtil.renderList("Test_A4",
                new HtmlRenderData(paramMaps, detailList), 4,
                new JasperReportParametersCover<Map<String, Object>>() {
                    @Override
                    public Map<String, Object> cover(Map<String, Object> paramMap) throws Exception {
                        paramMap.put("TABLE_TITLE", "测试标题2");
                        Console.log("======> {}", paramMap);
                        return paramMap;
                    }
                }, null);
        System.out.println("==================== JasperReport模板渲染后的Html代码 ====================");
        Console.log(htmlList);

        // 打印参数配置
        PrintConfig printConfig = new PrintConfig(htmlList);

        System.out.println("==================== Lodop打印javascript脚本语句 ====================");
        // 生成打印控件需要的代码
        String str = new Printer(printConfig).printList();
        System.out.println(str);

        System.out.println("打印运行耗时 " + timer.intervalSecond() + " 秒");
    }

    // endregion

    // region ============================= ofd测试 =============================

    /**
     * 示例6 - jasperReport导出html使用示例
     *
     * @throws Exception 异常
     */
    @Test
    public void demo6() throws Exception {
        TimeInterval timer = DateUtil.timer();

        Map<String, Object> paramMaps = new HashMap<String, Object>(1);
        paramMaps.put("TABLE_TITLE", "测试标题");

        List<Map<String, ?>> detailList = new ArrayList<Map<String, ?>>(20);
        for (int i = 0; i < 20; i++) {
            Map<String, Object> tempMap = new HashMap<String, Object>();
            tempMap.put("FIELD_ACCOUNT", MockUtil.title(5));
            tempMap.put("FIELD_REAL_NAME", MockUtil.cname());
            detailList.add(tempMap);
        }

        // 模板渲染
        String html = HtmlRenderFactoryUtil.render("Test_A4",
                new HtmlRenderData(paramMaps, detailList), new JasperReportParametersCover<Map<String, Object>>() {
                    @Override
                    public Map<String, Object> cover(Map<String, Object> parameterMap) throws Exception {
                        return parameterMap;
                    }
                }, new JasperReportParametersCover<List<Map<String, ?>>>() {
                    @Override
                    public List<Map<String, ?>> cover(List<Map<String, ?>> dataList) throws Exception {
                        return dataList;
                    }
                });

        System.out.println("==================== JasperReport模板渲染后的Html代码 ====================");
        System.out.println(html);

        // 打印参数配置
        PrintConfig printConfig = new PrintConfig(html);

        // 生成打印控件需要的代码
        String str = new Printer(printConfig).print();

        System.out.println("==================== Lodop打印javascript脚本语句 ====================");
        System.out.println(str);

        System.out.println("打印运行耗时 " + timer.intervalSecond() + " 秒");
    }

    /**
     * 示例7 - jasperReport导出ofd示例
     *
     * @return
     * @author song_jx
     */
    @Test
    public void demo7() {
        TimeInterval timer = DateUtil.timer();

        Map<String, Object> paramMaps = new HashMap<String, Object>(1);
        paramMaps.put("TABLE_TITLE", "测试&bull;标题");

        List<Map<String, ?>> detailList = new ArrayList<Map<String, ?>>(20);
        for (int i = 0; i < 20; i++) {
            Map<String, Object> tempMap = new HashMap<String, Object>();
            tempMap.put("FIELD_ACCOUNT", MockUtil.title(5) + "&gt;");
            tempMap.put("FIELD_REAL_NAME", MockUtil.cname());
            detailList.add(tempMap);
        }

        // 模板渲染
        File pdfFile = OfdRenderFactoryUtil.render("Test_A4",
                new HtmlRenderData(paramMaps, detailList));

        Console.log("======> 文件路径 = {}", pdfFile.getAbsolutePath());

        System.out.println("打印运行耗时 " + timer.intervalSecond() + " 秒");
    }

    /**
     * 示例8 - html转ofd
     *
     * @return
     * @author song_jx
     */
    @Test
    public void demo8() {
        // 字体只处理宋体！！！！

        String s = "<!DOCTYPE html PUBLIC\"-//W3C//DTD XHTML 1.0 Transitional//EN\"\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\"><html><head><title></title><meta http-equiv=\"Content-Type\"content=\"text/html; charset=UTF-8\"/><style type=\"text/css\">a{text-decoration:none}</style></head><body text=\"#000000\"link=\"#000000\"alink=\"#000000\"vlink=\"#000000\"><table width=\"100%\"cellpadding=\"0\"cellspacing=\"0\"border=\"0\"><tr><td width=\"50%\">&nbsp;</td><td align=\"center\"><a name=\"JR_PAGE_ANCHOR_0_1\"></a><table class=\"jrPage\"cellpadding=\"0\"cellspacing=\"0\"border=\"0\"style=\"empty-cells: show; width: 595px; border-collapse: collapse; background-color: white;\"><tr valign=\"top\"style=\"height:0\"><td style=\"width:20px\"></td><td style=\"width:180px\"></td><td style=\"width:100px\"></td><td style=\"width:100px\"></td><td style=\"width:175px\"></td><td style=\"width:20px\"></td></tr><tr valign=\"top\"style=\"height:20px\"><td colspan=\"6\"></td></tr><tr valign=\"top\"style=\"height:40px\"><td colspan=\"2\"></td><td colspan=\"2\"style=\"text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 21px; line-height: 1.140625;\">测试标题</span></td><td colspan=\"2\"></td></tr><tr valign=\"top\"style=\"height:20px\"><td colspan=\"6\"></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625; font-weight: bold;\">帐号</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625; font-weight: bold;\">真实姓名</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">Vtvjo</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">席皂斋</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">Ltoix</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">连雪空</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">Jazsj</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">屈速</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">Tzans</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">谷梁成</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">Eqakb</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">寿呈</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">Izgmw</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">萧菜铣</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">Vizmk</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">隆噶</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">Nhbkv</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">宦譬</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">Hhpzy</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">郇欠放</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">Xyupx</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">蔺换稍</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">Usczd</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">祖玩该</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">Wkfkc</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">韶芥写</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">Qgrvq</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">綦毋恋</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">Dmkmb</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">鲁驯</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">Yzlsc</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">籍仿</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">Nfwkd</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">都筒</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">Nsegj</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">莘喇距</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">Mvnln</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">覃知</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">Tkbkb</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">谈酸</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">Zynak</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: 宋体; color: #000000; font-size: 18px; line-height: 1.140625;\">边捷</span></td><td></td></tr><tr valign=\"top\"style=\"height:132px\"><td colspan=\"6\"></td></tr></table></td><td width=\"50%\">&nbsp;</td></tr></table></body></html>";
        File file = OfdRenderFactoryUtil.htmlToOfd(s, Boolean.FALSE);
        Console.log("======> 文件路径1 = {}", file.getAbsolutePath());
        // 该 html 的 style中的 font-family: \'宋体\' 经过特殊处理，需注意
        String s1 = "<!DOCTYPE html PUBLIC\"-//W3C//DTD XHTML 1.0 Transitional//EN\"\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\"><html><head><title></title><meta http-equiv=\"Content-Type\"content=\"text/html; charset=UTF-8\"/><style type=\"text/css\">a{text-decoration:none}</style></head><body text=\"#000000\"link=\"#000000\"alink=\"#000000\"vlink=\"#000000\"><table width=\"100%\"cellpadding=\"0\"cellspacing=\"0\"border=\"0\"><tr><td width=\"50%\">&nbsp;</td><td align=\"center\"><a name=\"JR_PAGE_ANCHOR_0_1\"></a><table class=\"jrPage\"cellpadding=\"0\"cellspacing=\"0\"border=\"0\"style=\"empty-cells: show; width: 595px; border-collapse: collapse; background-color: white;\"><tr valign=\"top\"style=\"height:0\"><td style=\"width:20px\"></td><td style=\"width:180px\"></td><td style=\"width:100px\"></td><td style=\"width:100px\"></td><td style=\"width:175px\"></td><td style=\"width:20px\"></td></tr><tr valign=\"top\"style=\"height:20px\"><td colspan=\"6\"></td></tr><tr valign=\"top\"style=\"height:40px\"><td colspan=\"2\"></td><td colspan=\"2\"style=\"text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 21px; line-height: 1.140625;\">测试标题</span></td><td colspan=\"2\"></td></tr><tr valign=\"top\"style=\"height:20px\"><td colspan=\"6\"></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625; font-weight: bold;\">帐号</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625; font-weight: bold;\">真实姓名</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">Vtvjo</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">席皂斋</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">Ltoix</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">连雪空</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">Jazsj</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">屈速</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">Tzans</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">谷梁成</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">Eqakb</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">寿呈</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">Izgmw</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">萧菜铣</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">Vizmk</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">隆噶</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">Nhbkv</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">宦譬</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">Hhpzy</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">郇欠放</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">Xyupx</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">蔺换稍</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">Usczd</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">祖玩该</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">Wkfkc</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">韶芥写</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">Qgrvq</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">綦毋恋</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">Dmkmb</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">鲁驯</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">Yzlsc</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">籍仿</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">Nfwkd</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">都筒</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">Nsegj</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">莘喇距</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">Mvnln</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: '\\'宋体\\''; color: #000000; font-size: 18px; line-height: 1.140625;\">覃知</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">Tkbkb</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">谈酸</span></td><td></td></tr><tr valign=\"top\"style=\"height:30px\"><td></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">Zynak</span></td><td colspan=\"2\"style=\"border: 1px solid #000000; text-indent: 0px;  vertical-align: middle;text-align: center;\"><span style=\"font-family: \\'宋体\\'; color: #000000; font-size: 18px; line-height: 1.140625;\">边捷</span></td><td></td></tr><tr valign=\"top\"style=\"height:132px\"><td colspan=\"6\"></td></tr></table></td><td width=\"50%\">&nbsp;</td></tr></table></body></html>";
        File file1 = OfdRenderFactoryUtil.htmlToOfd(s1);
        Console.log("======> 文件路径2 = {}", file1.getAbsolutePath());
    }

    /**
     * 示例9 - 使用beetl模板引擎
     *
     * @throws Exception 异常
     */
    @Test
    public void demo9() throws Exception {
        TimeInterval timer = DateUtil.timer();

        Map<String, Object> paramMaps = new HashMap<String, Object>(1);
        paramMaps.put("title", "测试标题");
        paramMaps.put("date", "2020-01-01 至 2020-02-28");

        List<Map<String, Object>> dataList = new ArrayList<Map<String, Object>>(3);
        for (int i = 0; i < 5; i++) {
            HashMap<String, Object> stringObjectHashMap = new HashMap<>();
            stringObjectHashMap.put("orgPosition", "测试" + i);
            stringObjectHashMap.put("opinion", "测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试");
            stringObjectHashMap.put("createByName", "姓名_" + i);
            stringObjectHashMap.put("createDateStr", "2020-09-30");
            dataList.add(stringObjectHashMap);
        }
        paramMaps.put("dataList", dataList);
        // 模板渲染
        String html = HtmlRenderFactoryUtil.render("demo4.html", paramMaps);

        System.out.println("==================== beetl模板渲染后的Html代码 ====================");
        System.out.println(html);

        // 打印参数配置
        PrintConfig printConfig = new PrintConfig(html);

        // 生成打印控件需要的代码
        String str = new Printer(printConfig).print();

        System.out.println("==================== Lodop打印javascript脚本语句 ====================");
        System.out.println(str);

        System.out.println("打印运行耗时 " + timer.intervalSecond() + " 秒");
    }

    /**
     * 示例10 - 套打
     *
     * @throws Exception 异常
     */
    @Test
    public void demo10() throws Exception {
        TimeInterval timer = DateUtil.timer();

        Map<String, Object> paramMaps = new HashMap<String, Object>(1);
        paramMaps.put("TABLE_TITLE", "测试标题");

        List<Map<String, ?>> detailList = new ArrayList<Map<String, ?>>(20);
        for (int i = 0; i < 5; i++) {
            Map<String, Object> tempMap = new HashMap<String, Object>();
            tempMap.put("FIELD_ACCOUNT", MockUtil.title(5));
            tempMap.put("FIELD_REAL_NAME", MockUtil.cname());
            detailList.add(tempMap);
        }

        // 模板渲染
        List<String> htmlList = HtmlRenderFactoryUtil.renderList("Test_A4",
                new HtmlRenderData(paramMaps, detailList), 4,
                new JasperReportParametersCover<Map<String, Object>>() {
                    @Override
                    public Map<String, Object> cover(Map<String, Object> paramMap) throws Exception {
                        paramMap.put("TABLE_TITLE", "测试标题2");
                        Console.log("======> {}", paramMap);
                        return paramMap;
                    }
                }, null);
        System.out.println("==================== JasperReport模板渲染后的Html代码 ====================");
        Console.log(htmlList);

        // 打印参数配置
        PrintConfig printConfig = new PrintConfig(htmlList);

        System.out.println("==================== Lodop打印javascript脚本语句 ====================");
        // 生成打印控件需要的代码
        String str = new Printer(printConfig).printList();
        System.out.println(str);

        System.out.println("打印运行耗时 " + timer.intervalSecond() + " 秒");
    }

    // endregion

}
